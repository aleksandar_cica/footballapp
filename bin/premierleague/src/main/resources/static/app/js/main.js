var app = angular.module("xxx",['ngRoute']);
var api = angular.module('xxx');




app.config(['$routeProvider',function($routeProvider){
	$routeProvider
		.when("/",{templateUrl : '/app/html/home.html'})
		.when("/:name",{templateUrl : '/app/html/datalist.html'})
		.when("/Teams/season/:season",{templateUrl : '/app/html/Teams.html'})
		.when("/Games/season/:year",{templateUrl : '/app/html/home.html'})
		.when("/jedanKlub/:clubName",{templateUrl : '/app/html/club.html'})
		.when("/:name/update/:id",{templateUrl : '/app/html/updateItem.html'})
		.when("/players/set/:id",{templateUrl : '/app/html/updateItem.html'})
		.when("/players/createTransfer/:id",{templateUrl : '/app/html/createTransfer.html'})
    	.when("/:name/save",{templateUrl : '/app/html/updateItem.html'})
		.otherwise({redirectTo: '/'})
}]);

