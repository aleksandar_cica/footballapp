api.controller('Football', function($scope,$http,$location,$routeParams) {
	$scope.season=2018;
	$scope.Patch;
	$scope.url="";
	$scope.New={};
	$scope.pageSize=20;
	$scope.page=0;
	$scope.order="id";
	$scope.reverse=false;
	$scope.data=[];
	$scope.totalPages=1;
	$scope.filter="";
	$scope.allTeams=[]; 
	$scope.refrees=[];
	$scope.guestTeams=[];
	$scope.clubs=[];
	$scope.countries=[];
	$scope.mounth=13;
	$scope.club={};  
	$scope.team={}; 
	$scope.error; 
	$scope.showError=false;
	$scope.isUpdate=false;
	
	
	function getCombinedDateTime(date, time){
		  var datePart = date.getTime();
		  var timePart = time.getTime();
		  return datePart+timePart;
		}
	
	Array.prototype.create = function(length) {   
	    for(var i = 1; i <= length; i++) {
	        this.push(i);
	    }
	    return this;
	}
	
	Array.prototype.findById = function(id) {   
	    for(var i = 0; i < this.length; i++) {
	    	if(this[i]["id"]===id)
	           return i;
	    }
	    return -1;
	}
	
	Array.prototype.add = function(element) {   
		if(this.indexOf(element)==-1){
	        this.unshift(element);
		}
	    return this;
	}
	
	$scope.goBack=function(){		
		window.history.go(-1);
	}
	
	$scope.setLocation=function(x){
		$location.path(x);
	}
	
	$scope.filterDataByName=function(){
		$scope.page=0;
		getData();
	}
	
	$scope.filterPlayersByTeam=function(){
		$scope.page=0;
		getData();
	}
	
	$scope.filterDataByDate=function(){
		$scope.page=0; 
		getData();
	}  
	
	
	var getTeams = function() {
		$http.get("/Teams/getData").then(function success(res) {
			$scope.allTeams=res.data;
			$scope.refrees=res.headers("refree").split(",");
		}, function error(res) {
		});
	  } 
	
	getTeams();

	var getOne = function() {
		if(!$routeParams.id){
		$scope.isUpdate=false;
		}else{
		$http.get($location.url()).then(function success(res) {
			$scope.isUpdate=true;
			$scope.url=$location.url();
			$scope.New=res.data;
		}, function error(res) {
		});
	  } 
	}
	
	
	
	var getData = function() {
		 if($routeParams.id){
			getOne();	
			 }else{
		var pag=$scope.page+"/"+$scope.pageSize+"/"+$scope.order+"/"+$scope.reverse;
		 if($routeParams.clubName){
			$scope.url="/Teams?search="+$routeParams.clubName;
	     }else if($routeParams.year){
				$scope.url="/Games/filterdata/"+($scope.gameDate?$scope.gameDate:""+$routeParams.year)+
				"/"+($scope.allTeamsSelect?$scope.allTeamsSelect:"all")+"/"+$scope.page+"/"+$scope.pageSize;
		   }else if($routeParams.name=="players"){
			var pag=$scope.page+"/"+$scope.pageSize+"/"+$scope.order+"/"+$scope.reverse;
			$scope.url="/players/"+($scope.search?$scope.search:"all")+","+
			($scope.country?$scope.country:"all")+","+($scope.teamName?$scope.teamName:"all")+"/"+pag;
		}else if($routeParams.name=="Transfers"){
			$scope.url="/Transfers/"+$scope.page+"/"+$scope.pageSize;
		}else if($routeParams.name=="Refree"){
			$scope.url="/"+$routeParams.name+"/"+pag;
		}else if($location.url()=="/"){
			$scope.url="/Games/filterdata/"+($scope.gameDate?$scope.gameDate:"2019")+
			"/"+($scope.allTeamsSelect?$scope.allTeamsSelect:"all")+"/"+$scope.page+"/"+$scope.pageSize;
		}else{
			$scope.url=$location.url();
		}
		$http.get($scope.url).then(function success(res) {
			$scope.data=res.data;
			$scope.totalPages=res.headers("totalpages");
		    $scope.countries=res.headers("countries").split(",");
		    $scope.countries.unshift("all");
		}, function error(res) {
		});
	  } 
	}	

$scope.del = function (id){
	if(confirm("Are you sure?")==true){
		if(!$routeParams.name){
			$routeParams.name="Games";
		}
		$http.delete("/"+$routeParams.name+"/delete/"+id).then(
				function success(res){
					var index=$scope.data.findById(id);
					$scope.data.splice(index,1);
				},function error(res){
					if(res.status==403)
					 alert("deleting id forbidden");
					else
						alert("something went wrong")
						
				})	
	  }
	}


$scope.edit = function (object){
	if(!$routeParams.name || $routeParams.name=="Games"){
		$routeParams.name="Games";
		$scope.patch($scope.New);
	}else if($routeParams.name=="players"){
			$scope.patch({"country":$scope.New.country,"name":$scope.New.name,"lastName":$scope.New.lastName});
	}else{
	  $http.put($location.url(),object).then(
				function success(res){
					alert("updated");
					getData();
				},function error(res){
					$scope.showError=true;
					$scope.error=res.data;
				})	
	}
}

$scope.patch = function (patchObject){
	  $http.patch($location.url(),patchObject).then(
				function success(res){
					alert("updated");
				},function error(res){
					$scope.showError=true;
					$scope.error=res.headers("FORBIDDEN");				
				})	
	}

$scope.save = function (){
	if(!$routeParams.name || $routeParams.name=="Games"){
		$routeParams.name="Games";
		$scope.New.date=getCombinedDateTime($scope.datum, $scope.vreme);
	}
	  $http.post($location.url(),$scope.New).then(
				function success(res){
					alert("saved");
					for(var property in $scope.New){
						$scope.New[property]="";
					}
				},function error(res){			
					$scope.showError=true;
					$scope.error=res.data; 
				})	
	}
 $scope.filterByteam=function(){
	 $scope.page=0;
	 getData();   
 }
 
 $scope.changeCountry=function(){
	 getData();
 }

	$scope.next=function(){
		$scope.page=$scope.page+1;
		getData();
	}
	
	$scope.back=function(){
		$scope.page=$scope.page-1;
		getData();
	}
	
	$scope.goToPage=function(x){
		$scope.page=x;
		getData();
	}
	
	$scope.sort=function(x){
		$scope.order=x;
		getData();
		$scope.reverse=!$scope.reverse;
	}
	
	$scope.nupp=function(){
		$scope.pageSize=$scope.userPerPage;
		$scope.page=0; $scope.mou=13;
		getData();
	}
	getData();
});