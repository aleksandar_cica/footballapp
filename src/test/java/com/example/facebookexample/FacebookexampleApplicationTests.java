package com.example.facebookexample;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.example.facebookexample.model.FootballPlayer;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc

public class FacebookexampleApplicationTests {

    @Autowired
    private MockMvc mvc;

    @Test
    public void TestPlayer() throws Exception {
        this.mvc.perform(get("/players/find/7")).andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.name").value("playerCity"));
    }

    @Test
    public void TestGame() throws Exception {
        this.mvc.perform(get("/Coachs/find/100")).andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.name").value("trener"));
    }

    @Test
    public void TestCoach() throws Exception {
        this.mvc.perform(get("/Game/find/927")).andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.home.name").value("United"));
    }

    @Test
    public void TestManager() throws Exception {
        this.mvc.perform(get("/Refree/find/2")).andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.lastName").value("Clarck"));
    }

    @Test
    public void updateEmployeeAPI() throws Exception {
        mvc.perform(MockMvcRequestBuilders
                .put("/employees/delete/{id}", 2)
                .content(asJsonString(new FootballPlayer(2, "firstName2", "lastName2", "Portugal")))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.firstName").value("firstName2"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.lastName").value("lastName2"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.email").value("email2@mail.com"));
    }

    private byte[] asJsonString(Object object) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        return mapper.writeValueAsString(object).getBytes();
    }

    @Test
    public void deleteRefree() throws Exception {
        mvc.perform(MockMvcRequestBuilders.delete("/Refree/delete/{id}", 2))
                .andExpect(status().isOk());
    }

    @Test
    public void contextLoads() {
    }
}
