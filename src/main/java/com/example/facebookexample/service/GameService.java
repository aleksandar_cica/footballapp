package com.example.facebookexample.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Service;

import com.example.facebookexample.dtoModel.CubResultDto;
import com.example.facebookexample.dtoModel.TableResponse;
import com.example.facebookexample.mappers.ClubResultMapper;
import com.example.facebookexample.model.Game;
import com.example.facebookexample.repository.GamesRepository;
import com.example.facebookexample.utilities.DateRange;
import com.example.facebookexample.utilities.Paginator;

@Service
public class GameService extends AbstractService<Game, Integer> {

    @Autowired
    private GamesRepository gameRepository;

    public ResponseEntity<?> save(Game game) {
        if (game.getGuest().equals(game.getHome())) {
            return createBadRequest("Same teams");
        }

        String refereeTown = game.getMainReferee().getTown();
        if (refereeTown.equals(game.getHome().getCity())) {
            return createBadRequest("Referee is from same town like home team");
        }
        if (refereeTown.equals(game.getGuest().getCity())) {
            return createBadRequest("Referee is from same town like guest team");
        }
        int result = gameRepository.findByHomeIdAndGustIdAndYear(game.getHome().getId(), game.getGuest().getId(),
                game.getDate().getYear());
        if (result != 0) {
            return createBadRequest("Clubs already played this year");
        }
        Map<String, String> map = errors(game);
        if (map == null) {
            Game createdGame = gameRepository.save(game);
            game.setId(createdGame.getId());
            return ResponseEntity.ok(game);
        }
        return ResponseEntity.badRequest().body(map);
    }

    public ResponseEntity<Integer> update(Integer id, Game game) {
        int result = gameRepository.changeResult(game.getHomeGoals(), game.getGuestGoals(), id);
        if (result == 1) {
            return ResponseEntity.ok(result);
        }
        return ResponseEntity.notFound().build();
    }

    public ResponseEntity<TableResponse<Game>> findAll(Paginator paginator, DateRange dateRange, Integer clubId, Integer refereeId) {
        MapSqlParameterSource params
                = new MapSqlParameterSource();
        StringBuilder whereCondition = new StringBuilder("WHERE 1=1");
        if (clubId != null) {
            params.addValue("clubId", clubId);
            whereCondition.append(" AND (g.g_id = :clubId OR g.h_id = :clubId)");
        }
        if (refereeId != null) {
            params.addValue("refereeId", refereeId);
            whereCondition.append(
                    " AND g.main_referee_id = :refereeId");
        }
        if (dateRange.getStartDate() != null) {
            params.addValue("startDate", dateRange.getStartDate());
            whereCondition.append(" AND g.date > :startDate");
        }
        if (dateRange.getEndDate() != null) {
            params.addValue("endDate", dateRange.getEndDate());
            whereCondition.append(" AND g.date < :endDate");
        }

        String countQuery = "SELECT COUNT(*) FROM game g ";

        String query = "SELECT g.id, g.date, g.homegoals, g.guestgoals, h.id as h_id, h.name as h_name, ge.id as g_id, ge.name as g_name, "
                + "r.id as r_id , r.name as r_name, r.last_name as r_last_name from game g JOIN club h ON g.h_id = h.id join club ge ON "
                + "ge.id = g.g_id join main_referee r on r.id = g.main_referee_id " + whereCondition.toString() + " "
                + paginator.orderByClause();

        return createTableResponse(query, countQuery + whereCondition.toString(), params);
    }

    public ResponseEntity<List<Integer>> years() {
        List<Integer> years = gameRepository.years();
        return ResponseEntity.ok(years);
    }

    public ResponseEntity<List<CubResultDto>> goalsStatistics(Integer season) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("WITH homeStatistics as (Select h_id, AVG(homegoals) as avg from game WHERE Year(date) = :season group by h_id),");
        stringBuilder.append(" guestStatistics as (Select g_id, AVG(guestgoals) as avg from game WHERE Year(date) = :season group by g_id)");
        stringBuilder.append(" Select h.h_id as id, c.name as name, h.avg as avg_home, g.avg as avg_guest from homeStatistics h join guestStatistics g");
        stringBuilder.append(" on h.h_id = g.g_id join club c on c.id = h.h_id");
        List<CubResultDto> clubStatistics = namedParameterJdbcTemplate.query(stringBuilder.toString(),
                new MapSqlParameterSource("season", season), new ClubResultMapper());
        return ResponseEntity.ok(clubStatistics);
    }

    public ResponseEntity<Integer> findByHomeIdAndGustIdAndYear(int homeId, int guestId, int year) {
        int result = gameRepository.findByHomeIdAndGustIdAndYear(homeId, guestId, year);
        return ResponseEntity.ok(result);
    }
}
