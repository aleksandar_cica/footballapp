package com.example.facebookexample.service;

import java.util.Comparator;
import java.util.Map;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.example.facebookexample.model.Role;
import com.example.facebookexample.repository.RoleRepository;

@Service
public class RolesService {

    @Autowired
    private RoleRepository roleRepository;

    public ResponseEntity<Map<String, TreeSet<Role>>> findAll() {
        final Map<String, TreeSet<Role>> data = roleRepository.findAll().stream()
                .collect(Collectors.groupingBy(a -> a.getRole().name().split("_")[1], () -> new TreeMap<>(),
                        Collectors.toCollection(() -> new TreeSet<Role>(Comparator.comparing(a -> a.getRole().name())))));
        return ResponseEntity.ok(data);
    }

}
