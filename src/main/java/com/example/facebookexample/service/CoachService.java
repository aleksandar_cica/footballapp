package com.example.facebookexample.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Service;

import com.example.facebookexample.dtoModel.PersonDto;
import com.example.facebookexample.dtoModel.TableResponse;
import com.example.facebookexample.mappers.PersonMapper;
import com.example.facebookexample.model.Club;
import com.example.facebookexample.model.Coach;
import com.example.facebookexample.repository.CoachRepository;
import com.example.facebookexample.utilities.Paginator;

@Service
public class CoachService extends AbstractService<Coach, Integer> {

    @Autowired
    private CoachRepository repository;

    public ResponseEntity<?> changeCoach(Integer coachId, Integer clubId) {
        return repository.findById(coachId).map(coach -> {
            if (coach.getClub() != null) {
                return createBadRequest("Coach is not free");
            }
            repository.releaseCoach(clubId);
            coach.setClub(new Club(clubId));
            repository.save(coach);
            return ResponseEntity.ok(coach);
        }).orElse(ResponseEntity.notFound().build());
    }

    public ResponseEntity<TableResponse<Coach>> findAll(Paginator paginator, String filter, List<String> countries, Integer clubId, boolean onlyFree) {
        StringBuilder where = new StringBuilder();
        if (paginator.getColumn().equalsIgnoreCase("clubName")) {
            paginator.setColumn("cl.name");
        }
        MapSqlParameterSource namedParameters
                = new MapSqlParameterSource();
        if (filter != null && !filter.isBlank()) {
            where.append(
                    " AND (LOWER(concat(c.name, ' ', c.last_name)) like :key OR LOWER(concat(c.last_name, ' ', c.name)) like :key)");
            namedParameters.addValue("key", "%" + filter.toLowerCase() + "%");
        }
        if (countries != null && !countries.isEmpty()) {
            where.append(" AND c.country in (:countries)");
            namedParameters.addValue("countries", countries);
        }

        if (clubId != null) {
            where.append(" AND cl.id = :clubId");
            namedParameters.addValue("clubId", clubId);
        } else if (onlyFree) {
            where.append(" AND cl.id IS NULL");
        }
        String query = "SELECT c.id, c.name, c.last_name, c.country, cl.id as club_id, cl.name as club_name FROM coach c LEFT JOIN club cl ON c.club_id = cl.id WHERE c.active = true" + where.toString() + " " + paginator.orderByClause();
        String countQuery = "SELECT COUNT(*) FROM coach c LEFT JOIN club cl ON c.club_id = cl.id WHERE c.active = true" + where.toString();
        return createTableResponse(query, countQuery, namedParameters);

    }

    public ResponseEntity<?> delete(Integer id) {
        return repository.findById(id).map(coach -> {
            coach.setActive(false);
            coach.setClub(null);
            repository.save(coach);
            return ResponseEntity.ok().build();
        }).orElse(ResponseEntity.notFound().build());

    }

    public ResponseEntity<List<PersonDto>> findByName(String keyword, Integer limit, Integer offset) {
        MapSqlParameterSource namedParameters
                = new MapSqlParameterSource("keyword", "%" + keyword.toLowerCase() + "%");
        List<PersonDto> persons = namedParameterJdbcTemplate.query(cerateQuery("coach", limit, offset), namedParameters, new PersonMapper());
        return ResponseEntity.ok(persons);
    }

}
