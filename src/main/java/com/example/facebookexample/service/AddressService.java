package com.example.facebookexample.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Service;

import com.example.facebookexample.dtoModel.AddressDto;
import com.example.facebookexample.mappers.AddressMapper;

@Service
public class AddressService {

    @Autowired
    NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    public ResponseEntity<List<AddressDto>> countries() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("SELECT distinct(country) as country, 'coach' as entity FROM coach WHERE active=true");
        stringBuilder.append(" UNION SELECT distinct(country) as country, 'player' as entity FROM football_player WHERE active=true order by country");

        List<AddressDto> result = namedParameterJdbcTemplate.query(
                stringBuilder.toString(),
                new AddressMapper());
        return ResponseEntity.ok(result);
    }

    public ResponseEntity<List<AddressDto>> cities() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("SELECT distinct(town) as city, 'referee' as entity FROM main_referee WHERE active=true");
        stringBuilder.append(" UNION SELECT distinct(city), 'club' FROM club order by city");

        List<AddressDto> result = namedParameterJdbcTemplate.query(
                stringBuilder.toString(),
                new AddressMapper());
        return ResponseEntity.ok(result);
    }
}
