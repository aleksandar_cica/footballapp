package com.example.facebookexample.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.facebookexample.InitClass;
import com.example.facebookexample.dtoModel.TableResponse;
import com.example.facebookexample.mappers.AdminUserResultSetExtractor;
import com.example.facebookexample.model.Admin;
import com.example.facebookexample.model.AdminUser;
import com.example.facebookexample.model.User;
import com.example.facebookexample.repository.AdminUserRepository;
import com.example.facebookexample.utilities.Paginator;

import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;

@Service
public class UserService extends AbstractService<AdminUser, Integer> {

    @Value("${token.durationInHours}")
    private Integer durationInHours;

    @Autowired
    private AdminUserRepository repository;

    @Autowired
    JdbcTemplate jdbcTemplate;

    public ResponseEntity<AdminUser> findById(Integer id) {
        List<AdminUser> list = findAllUsers(null, null, false, id, null);
        if (list == null || list.isEmpty()) {
            return ResponseEntity.notFound().build();
        }
        AdminUser a = list.get(0);
        a.setToken(null);
        return ResponseEntity.ok(a);
    }

    public ResponseEntity<?> updateImage(Integer id, String image) {
        int result = repository.updateImage(id, image == null || "null".equals(image) ? null : image.getBytes());
        if (result == 1) {
            return this.getImage(id);
        }
        return ResponseEntity.badRequest().build();
    }

    public ResponseEntity<?> updateTablesConfiguration(Integer id, String tableConfigurations) {
        int result = repository.updateTablesConfiguration(id, tableConfigurations);
        if (result == 1) {
            return ResponseEntity.ok(result);
        }
        return ResponseEntity.badRequest().build();
    }

    public ResponseEntity<?> getImage(Integer id) {
        byte[] bytes = repository.getImageById(id);
        return ResponseEntity.ok(Map.of("image", bytes == null ? "" : new String(bytes)));
    }

    public List<AdminUser> findAllUsers(Paginator paginator, String username, boolean exactUsername, Integer userId, String token) {
        StringBuilder stringBuilder = new StringBuilder();
        List<Object> params = new ArrayList<>();
        String where;
        if (userId != null) {
            where = "id = ? ";
            params.add(userId);
        } else if (token != null) {
            where = "token = ? ";
            params.add(token);
        } else {
            where = exactUsername ? "email = ? " : "LOWER(email) like LOWER(CONCAT('%', ? , '%')) ";
            params.add(username);
        }

        stringBuilder.append("WITH users as (SELECT * FROM admin_user  WHERE ").append(where)
                .append(paginator != null ? paginator.orderByClause() : "").append(") SELECT u.id, u.token").append(exactUsername ? " ,u.password" : "").append(" ,u.email, u.tables_configuration, u.dtype, r.id as role_id, r.role as role_name ")
                .append("  FROM users u LEFT JOIN user_roles ur ON u.id = ur.admin_user_id LEFT JOIN role r ON r.id = ur.role_id");

        List<AdminUser> list = jdbcTemplate.query(stringBuilder.toString(),
                params.toArray(),
                new AdminUserResultSetExtractor()
        );
        return list;
    }

    public ResponseEntity<TableResponse<AdminUser>> findAll(Paginator paginator, String username) {
        List<AdminUser> list = findAllUsers(paginator, username, false, null, null);
        Long count
                = jdbcTemplate.queryForObject(
                        "Select COUNT(*) from admin_user a WHERE LOWER(a.email) like LOWER(CONCAT('%', ? , '%'))",
                        new Object[]{username}, Long.class);

        return ResponseEntity.ok(new TableResponse<>(count, list));
    }

    public ResponseEntity<?> save(User user, String type) {
        AdminUser a;
        if ("admin".equalsIgnoreCase(type)) {
            a = new Admin();
        } else {
            a = new User();
        }
        a.setEmail(user.getEmail());
        a.setPassword(user.getPassword());
        Map<String, String> errors = errors(a);
        if (errors == null) {
            a.setPassword(InitClass.passwordEncoder().encode(user.getPassword()));
            AdminUser saved = repository.save(a);
            user.setId(saved.getId());
            return ResponseEntity.ok(user);
        }
        return ResponseEntity.badRequest().body(errors);
    }

    @Transactional
    public ResponseEntity<?> updateRoles(Integer adminUserId, List<Integer> roleIds) {
        repository.deleteUserRolesByUserId(adminUserId);
        if (roleIds != null && !roleIds.isEmpty()) {
            roleIds.forEach(roleId -> {
                repository.insertUserRole(adminUserId, roleId);
            });
        }
        return ResponseEntity.ok().build();
    }

    public ResponseEntity<?> login(AdminUser adminUser) {
        this.validateCredentials(adminUser);
        List<AdminUser> list = findAllUsers(null, adminUser.getEmail(), true, null, null);
        if (list == null || list.isEmpty()) {
            return ResponseEntity.notFound().build();
        }
        AdminUser user = list.get(0);
        boolean match = InitClass.passwordEncoder().matches(adminUser.getPassword(), user.getPassword());
        if (!match) {
            return ResponseEntity.notFound().build();
        }
        String token = createJWT(user.getEmail());
        user.setToken(token);
        repository.changeToken(user.getId(), token);
        return ResponseEntity.ok(user);
    }

    public ResponseEntity<?> changePassword(AdminUser adminUser) {
        this.validateCredentials(adminUser);
        if (!adminUser.getPassword().equals(adminUser.getConfirmPassword())) {
            return ResponseEntity.badRequest().body(Map.of("error", "Password and confirm password don't match"));
        }
        List<AdminUser> list = findAllUsers(null, adminUser.getEmail(), true, null, null);
        if (list == null || list.isEmpty()) {
            return ResponseEntity.notFound().build();
        }
        repository.changePassword(list.get(0).getId(), InitClass.passwordEncoder().encode(adminUser.getPassword()));
        return ResponseEntity.ok().build();
    }

    private void validateCredentials(AdminUser credentials) {
        if (credentials.getEmail() == null || credentials.getEmail().isBlank()
                || credentials.getPassword() == null || credentials.getPassword().isBlank()) {
            throw new UsernameNotFoundException("Password and email must be provided");
        }
    }

    private String createJWT(String email) {
        JwtBuilder builder = Jwts.builder();
        builder.setId(email);
        builder.setExpiration(new Date(new Date().getTime() + 3600 * 1000 * durationInHours));
        return builder.compact();
    }

    public ResponseEntity<?> logout(AdminUser adminUser) {
        repository.changePassword(adminUser.getId(), null);
        return ResponseEntity.ok().build();
    }
}
