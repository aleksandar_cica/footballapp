package com.example.facebookexample.service;

import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Service;

import com.example.facebookexample.dtoModel.TableResponse;
import com.example.facebookexample.mappers.RefereeMapper;
import com.example.facebookexample.model.MainReferee;
import com.example.facebookexample.utilities.Paginator;

@Service
public class RefereeService extends AbstractService<MainReferee, Integer> {

    public ResponseEntity<TableResponse<MainReferee>> findAll(Paginator pr, String filter, List<String> towns) {
        StringBuilder where = new StringBuilder();
        MapSqlParameterSource namedParameters
                = new MapSqlParameterSource();
        if (filter != null && !filter.isBlank()) {
            where.append(
                    " AND (LOWER(concat(name, ' ', last_name)) like :key OR LOWER(concat(last_name, ' ', name)) like :key)");
            namedParameters.addValue("key", "%" + filter.toLowerCase() + "%");
        }
        if (towns != null && !towns.isEmpty()) {
            where.append(" AND town in (:towns)");
            namedParameters.addValue("towns", towns);
        }

        return createTableResponse("SELECT * FROM main_referee WHERE active = true " + where.toString() + " " + pr.orderByClause(),
                "SELECT COUNT(*) FROM main_referee WHERE active = true" + where.toString(), namedParameters);

    }

    public ResponseEntity<List<MainReferee>> select(String keyword, Integer limit, Integer offset) {
        MapSqlParameterSource namedParameters
                = new MapSqlParameterSource("keyword", "%" + keyword.toLowerCase() + "%");
        List<MainReferee> persons = namedParameterJdbcTemplate.query(cerateQuery("main_referee", limit, offset), namedParameters, new RefereeMapper());
        return ResponseEntity.ok(persons);
    }

}
