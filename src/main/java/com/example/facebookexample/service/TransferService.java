package com.example.facebookexample.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Service;

import com.example.facebookexample.dtoModel.ClubTransfersBalance;
import com.example.facebookexample.dtoModel.TableResponse;
import com.example.facebookexample.mappers.TransferBalanceMapper;
import com.example.facebookexample.model.Club;
import com.example.facebookexample.model.Transfer;
import com.example.facebookexample.repository.FootballPlayerRepository;
import com.example.facebookexample.repository.TransferRepository;
import com.example.facebookexample.utilities.DateRange;
import com.example.facebookexample.utilities.Maps;
import com.example.facebookexample.utilities.Paginator;

@Service
public class TransferService extends AbstractService<Transfer, Integer> {

    @Autowired
    private FootballPlayerRepository playerRepository;
    @Autowired
    private TransferRepository transferRepository;

    public ResponseEntity<TableResponse<Transfer>> findAll(Paginator paginator, Integer clubId, Integer playerId, Boolean onlyActivePlayers, String playerName, DateRange dateRange) {
        MapSqlParameterSource params
                = new MapSqlParameterSource();
        StringBuilder whereCondition = new StringBuilder("WHERE 1=1");
        if (clubId != null) {
            params.addValue("clubId", clubId);
            whereCondition.append(" AND (t.sailer_id = :clubId OR t.buyer_id = :clubId)");
        }
        if (playerName != null && !playerName.isBlank()) {
            params.addValue("key", "%" + playerName.toLowerCase() + "%");
            whereCondition.append(
                    " AND (LOWER(concat(p.name, ' ', p.last_name)) like :key OR LOWER(concat(p.last_name, ' ', p.name)) like :key)");
        }
        if (playerId != null) {
            params.addValue("playerId", playerId);
            whereCondition.append(" AND t.player_id = :playerId");
        }
        if (Boolean.TRUE.equals(onlyActivePlayers)) {
            params.addValue("onlyActivePlayers", true);
            whereCondition.append(" AND p.active = true");
        }
        if (dateRange.getStartDate() != null) {
            params.addValue("startDate", dateRange.getStartDate());
            whereCondition.append(" AND t.date > :startDate");

        }

        if (dateRange.getEndDate() != null) {
            params.addValue("endDate", dateRange.getEndDate());
            whereCondition.append(" AND t.date < :endDate");
        }

        String countQuery = "SELECT COUNT(*) FROM transfers t JOIN football_player p ON p.id = t.player_id ";

        String query = "SELECT t.date as transferDate, t.money as money, p.id, p.name, p.last_name,"
                + " p.active, b.id as buyerId, b.name as buyerName, s.id as sailerId, s.name as sailerName from transfers t"
                + " join football_player p on p.id = t.player_id join club b on b.id = t.buyer_id left join club s on s.id = t.sailer_id " + whereCondition.toString()
                + paginator.orderByClause();

        return createTableResponse(query, countQuery + whereCondition.toString(), params);
    }

    public ResponseEntity<List<ClubTransfersBalance>> transferBalanceBySeason(Paginator paginator, Integer season) {
        MapSqlParameterSource params
                = new MapSqlParameterSource();
        StringBuilder whereCondition = new StringBuilder("WHERE 1=1");
        if (season != null) {
            params.addValue("season", season);
            whereCondition.append(" AND YEAR(date) = :season");
        }

        String query = "WITH statistics as "
                + "(WiTH incomes as (select sum(money) as incomes, sailer_id as id from transfers " + whereCondition.toString() + " group by sailer_id),"
                + " outcomes as (select sum(money) as outcomes, buyer_id as id from transfers " + whereCondition.toString() + " group by buyer_id),"
                + " clubIds as (select distinct(id) as id from club)"
                + " Select clubIds.id as id, c.name, i.incomes, o.outcomes from clubIds left join club c on c.id = clubIds.id"
                + " left join incomes i on c.id = i.id left join outcomes o on o.id = c.id)"
                + " Select id as clubId, name as clubName, coalesce(incomes,0) as incomes, coalesce(outcomes,0) as outcomes, coalesce(incomes,0) - coalesce(outcomes,0)"
                + " as diff from statistics " + paginator.orderByClause();

        List<ClubTransfersBalance> result = namedParameterJdbcTemplate.query(query, params, new TransferBalanceMapper());

        return ResponseEntity.ok(result);

    }

    public ResponseEntity<?> save(Transfer transfer) {
        return playerRepository.findByActiveTrueAndId(transfer.getPlayer().getId()).map(player -> {
            Integer currentClubId = transferRepository.getClubIdByPlayerId(player.getId());
            if (currentClubId != null) {
                transfer.setSailerClub(new Club(currentClubId));
            }
            if (transfer.getBuyerClub().equals(transfer.getSailerClub())) {
                return ResponseEntity.badRequest().body(Maps.of("error", "Some clubs"));
            }
            Transfer result = transferRepository.save(transfer);
            transfer.setId(result.getId());
            return ResponseEntity.ok(transfer);
        }).orElse(ResponseEntity.notFound().build());
    }
}
