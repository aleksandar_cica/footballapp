package com.example.facebookexample.service;

import java.io.File;
import java.io.Serializable;
import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.net.URL;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import javax.persistence.Column;
import javax.persistence.EntityManager;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.validation.ConstraintViolation;
import javax.validation.Validator;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ReflectionUtils;

import com.example.facebookexample.annotations.DeleteModification;
import com.example.facebookexample.dtoModel.TableResponse;
import com.example.facebookexample.utilities.Paginator;
import com.example.facebookexample.utilities.PropertiesSetter;

public abstract class AbstractService<M, ID extends Serializable> {

    @Autowired
    private Validator validator;

    @Autowired
    NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    @Autowired
    private EntityManager entityManager;

    public M patch(M model, Map<String, Object> newValues) {
        if (!PropertiesSetter.patch(model, newValues)) {
            return null;
        }
        return model;
    }

    public ResponseEntity<TableResponse<M>> createQuery(List<String> columns, Paginator paginator,
            Specification<M> specification) {
        List<M> data = getDataBySpecification(columns, paginator, specification);
        long count = countBySpecification(specification);
        return ResponseEntity.ok(new TableResponse<>(count, data));
    }

    public List<M> getDataBySpecification(List<String> columns, Paginator paginator,
            Specification<M> specification) {
        Class<M> modelClass = this.getModelClass();
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<M> query = cb.createQuery(modelClass);
        Root<M> root = query.from(modelClass);
        @SuppressWarnings("unchecked")
        final Path<M>[] paths = new Path[columns.size()];
        for (int i = 0; i < columns.size(); i++) {
            paths[i] = this.getExpression(columns.get(i), root);
        }
        query.multiselect(paths);
        query.where(specification.toPredicate(root, query, cb));
        if (paginator != null) {
            if (paginator.getColumn() != null) {
                Path<M> orderByPath = this.getExpression(paginator.getColumn(), root);
                query.orderBy(paginator.isReverse() ? cb.desc(orderByPath) : cb.asc(orderByPath));
            }
            return entityManager.createQuery(query)
                    .setFirstResult(paginator.getOffset())
                    .setMaxResults(paginator.getPageSize())
                    .getResultList();
        }
        return entityManager.createQuery(query).getResultList();

    }

    public Long countBySpecification(Specification<M> spec) {
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Long> query = builder.createQuery(Long.class);
        Root<M> root = query.from(this.getModelClass());
        query.where(spec.toPredicate(root, query, builder));
        query.select(builder.count(root));
        return entityManager.createQuery(query).getSingleResult();
    }

    @Transactional
    public ResponseEntity<M> saveEntity(M model) {
        Map<String, String> errors = this.errors(model);
        if (errors == null) {
            insertInDataBase(model);
            if (model == null) {
                return ResponseEntity.badRequest().build();
            }
            return ResponseEntity.ok(model);
        }
        return ResponseEntity.badRequest().build();
    }

    public Specification<M> createIdSpecifications(ID id) {
        return (root, query, criteriaBuilder) -> {
            List<Predicate> predicates = new ArrayList<>();
            predicates.add(criteriaBuilder.equal(root.get(this.getIdColumnName(this.getModelClass())), id));
            return criteriaBuilder.and(predicates.toArray(new Predicate[0]));
        };
    }

    public Optional<M> findById(ID id, List<String> columns) {
        List<M> result = getDataBySpecification(columns, null, this.createIdSpecifications(id));
        if (result == null || result.isEmpty()) {
            return Optional.empty();
        }
        return Optional.of(result.get(0));
    }

    public boolean existsById(ID id) {
        Long count = countBySpecification(this.createIdSpecifications(id));
        return Long.valueOf(1).equals(count);
    }

    @Transactional
    public ResponseEntity<M> updateEntity(M model, ID id) {
        if (!existsById(id)) {
            return ResponseEntity.notFound().build();
        }
        Map<String, String> errors = this.errors(model);
        if (errors == null) {
            updateInDataBase(model);
            if (model == null) {
                return ResponseEntity.badRequest().build();
            }
            return ResponseEntity.ok(model);
        }
        return ResponseEntity.badRequest().build();
    }

    public void updateInDataBase(M model) {
        entityManager.merge(model);
    }

    public void insertInDataBase(M model) {
        entityManager.persist(model);
    }

    @Transactional
    public ResponseEntity<Integer> deleteById(ID id) {
        Class<M> modelClass = getModelClass();
        String deleteQuery;
        DeleteModification deleteAnnotation = modelClass.getAnnotation(DeleteModification.class);
        if (deleteAnnotation != null) {
            String idColumn = getIdColumnName(modelClass);
            String entityName = getTableName(modelClass);
            String updatingColumn = deleteAnnotation.field();
            Object deleteValue = null;

            MapSqlParameterSource params = new MapSqlParameterSource();
            if (!deleteAnnotation.delete()) {
                Class<?> updatingColumnClass = getColumnClass(modelClass, deleteAnnotation.field());
                switch (updatingColumnClass.getName()) {
                    case "boolean":
                        deleteValue = Boolean.valueOf(deleteAnnotation.deleteValue());
                        break;
                    case "int":
                        deleteValue = Integer.valueOf(deleteAnnotation.deleteValue());
                        break;
                    case "short":
                        deleteValue = Short.valueOf(deleteAnnotation.deleteValue());
                        break;
                    case "byte":
                        deleteValue = Byte.valueOf(deleteAnnotation.deleteValue());
                        break;
                    default:
                        break;
                }
                deleteQuery = "UPDATE " + entityName + " SET " + updatingColumn + " = :newValue WHERE " + idColumn
                        + " = :id";
                params.addValue("newValue", deleteValue);

            } else {
                deleteQuery = "DELETE FROM " + entityName + " WHERE " + idColumn + " = :id";
            }
            params.addValue("id", id);
            int result = namedParameterJdbcTemplate.update(deleteQuery, params);
            if (result == 1) {
                return ResponseEntity.ok(result);
            }
            return ResponseEntity.notFound().build();
        }
        return null;
    }

    public String getIdColumnName(Class<?> entityClass) {
        while (!entityClass.getClass().equals(Object.class)) {
            for (Field field : entityClass.getDeclaredFields()) {
                // Check if the field is annotated with @Id
                if (field.isAnnotationPresent(Id.class)) {
                    // Check if the @Column annotation is present
                    if (field.isAnnotationPresent(Column.class)) {
                        // Get the @Column annotation
                        Column columnAnnotation = field.getAnnotation(Column.class);
                        // Return the name of the column if specified
                        if (!columnAnnotation.name().isEmpty()) {
                            return columnAnnotation.name();
                        }
                    }
                    // If no @Column annotation is present, return the field name as the column name
                    return field.getName();
                }
            }
            return getIdColumnName(entityClass.getSuperclass());
        }
        throw new RuntimeException("No field with @Id annotation found in class: " + entityClass.getName());
    }

    public Class<?> getColumnClass(Class<?> entityClass, String column) {
        while (!entityClass.getClass().equals(Object.class)) {
            for (Field field : entityClass.getDeclaredFields()) {
                if (field.getName().equals(column)) {
                    if (field.getType().isPrimitive()) {
                        getWrapperClass(field.getType());
                    }
                    return field.getType();
                }
            }
            return getColumnClass(entityClass.getSuperclass(), column);
        }
        throw new RuntimeException("No field with name " + column + " found in class: " + entityClass.getName());
    }

    public String getTableName(Class<?> entityClass) {
        // Check if the entity class has the @Table annotation
        if (entityClass.isAnnotationPresent(Table.class)) {
            // Get the @Table annotation
            Table tableAnnotation = entityClass.getAnnotation(Table.class);
            if (!tableAnnotation.name().isBlank()) {
                return tableAnnotation.name();
            }
        }
        // If @Table annotation is not present, return the class name as the table name
        return convertCamelToSnake(entityClass.getSimpleName());
    }

    @SuppressWarnings({"deprecation", "unchecked"})
    public RowMapper<M> getRowMapper() throws ClassNotFoundException {
        String packageName = "com.example.facebookexample.mappers";
        String path = packageName.replace('.', '/');
        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
        URL resource = classLoader.getResource(path);
        File directory = new File(resource.getFile());

        if (directory.exists()) {
            File[] files = directory.listFiles();
            for (File file : files) {
                String className = packageName + '.' + file.getName().substring(0, file.getName().length() - 6);
                Class<?> cl = Class.forName(className);
                for (Type type : cl.getGenericInterfaces()) {
                    // Check if it's a parameterized type and is of type RowMapper
                    if (type instanceof ParameterizedType) {
                        ParameterizedType parameterizedType = (ParameterizedType) type;
                        if (parameterizedType.getRawType().equals(RowMapper.class)) {
                            Type c = parameterizedType.getActualTypeArguments()[0];
                            if (c.equals(getModelType())) {
                                try {
                                    return (RowMapper<M>) cl.newInstance();
                                } catch (InstantiationException | IllegalAccessException e) {
                                }
                            }
                        }
                    }
                }
            }
        }
        return null;
    }

    public ResponseEntity<TableResponse<M>> createTableResponse(String query, String countingQuery,
            MapSqlParameterSource params) {
        RowMapper<M> rowMapper;
        try {
            rowMapper = getRowMapper();
        } catch (ClassNotFoundException e) {
            return ResponseEntity.badRequest().build();
        }
        if (rowMapper == null) {
            return ResponseEntity.badRequest().build();
        }
        List<M> result = namedParameterJdbcTemplate.query(query, params, rowMapper);

        Long count = namedParameterJdbcTemplate.queryForObject(countingQuery, params, Long.class);

        return ResponseEntity.ok(new TableResponse<>(count, result));
    }

    @SuppressWarnings("unchecked")
    private Class<M> getModelClass() {
        Type type = getClass().getGenericSuperclass();
        ParameterizedType paramType = (ParameterizedType) type;
        return (Class<M>) paramType.getActualTypeArguments()[0];
    }

    private Type getModelType() {
        Type type = getClass().getGenericSuperclass();
        ParameterizedType paramType = (ParameterizedType) type;
        return paramType.getActualTypeArguments()[0];
    }

    @SuppressWarnings("deprecation")
    protected M createInstance() {
        Class<M> claszz = getModelClass();
        try {
            return claszz.newInstance();
        } catch (InstantiationException | IllegalAccessException e) {
        }
        return null;
    }

    protected Map<String, String> errors(M model) {
        Map<String, String> map = new LinkedHashMap<>();
        Set<ConstraintViolation<M>> errors = validator.validate(model);
        if (!errors.isEmpty()) {
            for (ConstraintViolation<M> e : errors) {
                map.put(e.getPropertyPath().toString(), e.getMessage());
            }
            return map;
        }
        return null;
    }

    @Bean
    protected ModelMapper modelMapper() {
        return new ModelMapper();
    }

    protected void sort(Root<M> root, CriteriaQuery<M> query, CriteriaBuilder criteriaBuilder, String order,
            boolean direction) {
        if (order == null || order.isEmpty()) {
            return;
        }
        Path<M> expression = this.getExpression(order, root);
        if (direction) {
            query.orderBy(criteriaBuilder.desc(expression));
        } else {
            query.orderBy(criteriaBuilder.asc(expression));
        }
    }

    protected Path<M> getExpression(String e, Root<M> root) {
        String[] parts = e.split("\\."); // Split on the dot (.)
        Path<M> path = root;

        for (String part : parts) {
            path = path.get(part);
        }

        return path;
    }

    @SuppressWarnings("null")
    public M patchUtil(M object, Map<String, Object> map) {
        map.forEach((k, v) -> {
            try {
                Field f = object.getClass().getField(k);

                if (f.getType() == Byte.class) {
                    map.put(k,
                            (byte) v
                    );
                } else if (f.getType() == Short.class) {
                    map.put(k,
                            (short) v
                    );
                }
            } catch (NoSuchFieldException | SecurityException e) {
            }
            Field field = ReflectionUtils.findField(getModelClass(), k);
            ReflectionUtils.setField(field, object, v);
        });
        return object;
    }

    public ResponseEntity<?> createBadRequest(String message) {
        return ResponseEntity.badRequest().body(Map.of("error", message));
    }

    public String cerateQuery(String table, Integer limit, Integer offset) {
        return "SELECT id, name, last_name " + ("main_referee".equals(table) ? ", town" : "") + " FROM " + table
                + " WHERE active = true AND"
                + ("coach".equalsIgnoreCase(table) ? " club_id IS NULL AND" : "")
                + " (LOWER(CONCAT(name, ' ', last_name)) LIKE :keyword OR LOWER(CONCAT(last_name, ' ', name)) LIKE :keyword)"
                + List.of("", "order by name limit", "" + limit, "offset", "" + offset).stream()
                        .collect(Collectors.joining(" "));
    }

    public static Class<?> getWrapperClass(Class<?> primitiveClass) {
        switch (primitiveClass.getName().toLowerCase()) {
            case "int":
                return Integer.class;
            case "double":
                return Double.class;
            case "boolean":
                return Boolean.class;
            case "long":
                return Long.class;
            case "float":
                return Float.class;
            case "char":
                return Character.class;
            case "byte":
                return Byte.class;
            case "short":
                return Short.class;
            case "void":
                return Void.class;
            default:
                break;
        }
        throw new IllegalArgumentException("Not a primitive type: " + primitiveClass);
    }

    public String convertCamelToSnake(String camelCase) {
        if (camelCase == null || camelCase.isBlank()) {
            return camelCase;
        }
        String result = camelCase.replaceAll("([a-z])([A-Z])", "$1_$2");
        return result.toLowerCase();
    }
}
