package com.example.facebookexample.service;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import com.example.facebookexample.dtoModel.CubResultDto;
import com.example.facebookexample.mappers.ClubResultMapper;
import com.example.facebookexample.model.Club;
import com.example.facebookexample.repository.ClubRepository;

@Service
public class ClubService {

    @Autowired
    private ClubRepository repository;

    @Autowired
    JdbcTemplate jdbcTemplate;

    public ResponseEntity<List<CubResultDto>> findBySeason(int season, String column, boolean reverse) {
        String direction = reverse ? " DESC" : " ASC";
        String orderBy = column + direction;
        if ("points".equals(column)) {
            orderBy += ", scoredGoals - receivedGoals " + direction;
        } else if ("goalDiff".equalsIgnoreCase(column)) {
            orderBy = "scoredGoals - receivedGoals " + direction;
        }
        LocalDateTime start = LocalDateTime.of(season, 1, 1, 0, 0);
        LocalDateTime end = start.plusYears(1);
        LocalDateTime now = LocalDateTime.now();
        end = end.isBefore(now) ? end : now;
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("WITH totalStatistics as (SELECT c.id, c.name, sum(points) as points, sum(scoredGoals) as scoredGoals, ");
        stringBuilder.append("sum(receivedGoals) as receivedGoals, sum(gamestotal) as totalGames from ");
        stringBuilder.append("(SELECT g1.h_id as teamId, sum(if(g1.homegoals > g1.guestgoals ");
        stringBuilder.append(", 3, if(g1.homegoals = g1.guestgoals, 1, 0))) as points, ");
        stringBuilder.append("sum(g1.homegoals) as scoredGoals , sum(g1.guestgoals) as receivedGoals , count(*) as gamestotal ");
        stringBuilder.append("from game g1 where g1.date > ? and g1.date < ? group by g1.h_id union all ");
        stringBuilder.append("SELECT g1.g_id, sum(if(g1.homegoals > g1.guestgoals, 0, if(g1.homegoals=g1.guestgoals, 1, 3))) as pints , ");
        stringBuilder.append("sum(g1.guestgoals) as scoredGoals ,sum(g1.homegoals) as receivedGoals ,count(*) as gamestotal ");
        stringBuilder.append("from game g1 where g1.date > ? and g1.date < ? group by g1.g_id) as statistic ");
        stringBuilder.append("left join club c on teamId=c.id group by teamId)");
        stringBuilder.append("SELECT *, scoredGoals - receivedGoals as diff from totalStatistics order by ");
        stringBuilder.append(orderBy);
        List<CubResultDto> result = jdbcTemplate.query(stringBuilder.toString(),
                new Object[]{start, end, start, end},
                new ClubResultMapper());
        return ResponseEntity.ok(result);
    }

    public ResponseEntity<List<Club>> getAllClubsNames() {
        List<Club> clubNames = repository.findByName("", 10000, 0);
        return ResponseEntity.ok(clubNames);
    }

    public ResponseEntity<Club> find(Integer id) {
        return repository.findById(id).map(club -> {
            return ResponseEntity.ok(club);
        }).orElse(ResponseEntity.notFound().build());
    }
}
