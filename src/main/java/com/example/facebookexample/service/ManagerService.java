package com.example.facebookexample.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Service;

import com.example.facebookexample.dtoModel.PersonDto;
import com.example.facebookexample.mappers.PersonMapper;
import com.example.facebookexample.model.FootballManager;
import com.example.facebookexample.repository.ManagerRepository;
import com.example.facebookexample.utilities.Paginator;

@Service
public class ManagerService extends AbstractService<FootballManager, Integer> {

    @Autowired
    private ManagerRepository managerRepository;

    public ResponseEntity<Page<FootballManager>> findAll(Paginator paginator, String name) {
        Page<FootballManager> resultList = managerRepository
                .findByNameOrLastNameLike("%" + name == null ? "" : name + "%", paginator.toPageRequest());
        return ResponseEntity.ok(resultList);
    }

    public ResponseEntity<List<PersonDto>> select(int limit, int offset, String keyword) {
        MapSqlParameterSource namedParameters
                = new MapSqlParameterSource("keyword", "%" + keyword.toLowerCase() + "%");
        List<PersonDto> persons = namedParameterJdbcTemplate.query(cerateQuery("football_manager", limit, offset), namedParameters, new PersonMapper());
        return ResponseEntity.ok(persons);
    }

}
