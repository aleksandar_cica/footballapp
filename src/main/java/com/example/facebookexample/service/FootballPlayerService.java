package com.example.facebookexample.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.facebookexample.dtoModel.PersonDto;
import com.example.facebookexample.dtoModel.TableResponse;
import com.example.facebookexample.mappers.FootballPlayerMapper;
import com.example.facebookexample.mappers.PersonMapper;
import com.example.facebookexample.model.Club;
import com.example.facebookexample.model.FootballManager;
import com.example.facebookexample.model.FootballPlayer;
import com.example.facebookexample.model.Transfer;
import com.example.facebookexample.repository.ClubRepository;
import com.example.facebookexample.repository.FootballPlayerRepository;
import com.example.facebookexample.repository.ManagerRepository;
import com.example.facebookexample.repository.TransferRepository;
import com.example.facebookexample.utilities.Paginator;

@Service
public class FootballPlayerService extends AbstractService<FootballPlayer, Integer> {

    @Autowired
    private FootballPlayerRepository playerRepository;
    @Autowired
    private ManagerRepository managerRepository;
    @Autowired
    private ClubRepository clubRepository;
    @Autowired
    private TransferRepository transferRepository;

    public ResponseEntity<List<PersonDto>> getPlayerForSelect(String filter, Integer limit, Integer offset) {
        String keyword = "%" + (filter == null || filter.isEmpty() ? "" : filter.toLowerCase()) + "%";
        MapSqlParameterSource namedParameters
                = new MapSqlParameterSource("keyword", keyword);
        List<PersonDto> persons = namedParameterJdbcTemplate.query(cerateQuery("football_player", limit, offset), namedParameters, new PersonMapper());
        return ResponseEntity.ok(persons);
    }

    public ResponseEntity<TableResponse<FootballPlayer>> findAll(Paginator paginator,
            String filter, List<String> countries, List<Integer> clubIds, List<String> positions, Integer managerId, Integer playerId) {
        String keyword = "%" + (filter == null || filter.isEmpty() ? "" : filter.toLowerCase()) + "%";
        StringBuilder stringBuilder = new StringBuilder();
        boolean isPlayerId = playerId != null;
        stringBuilder.append("WITH transfer_gr as (select buyer_id, player_id, ROW_NUMBER() OVER(PARTITION BY player_id ORDER BY date DESC) AS t_num FROM transfers)");
        stringBuilder.append(
                " SELECT p.id, p.name, p.last_name as lastName, p.country, p.position, p.active, c.id as clubId, c.name as clubName");
        if (isPlayerId) {
            stringBuilder.append(", m.id as managerId, m.name as managerName, m.last_name as managerLastName, m.country as managerCountry, m.active as managerActive");
        }
        stringBuilder.append(" FROM football_player p");
        stringBuilder.append(" LEFT JOIN transfer_gr t on t.player_id = p.id LEFT JOIN club c on c.id = t.buyer_id");
        if (isPlayerId) {
            stringBuilder.append(" LEFT JOIN football_manager m ON m.id = p.manager");
        }
        stringBuilder.append(" WHERE (t.t_num = 1 OR t.t_num is NULL) AND p.active = true");

        StringBuilder countingBuilder = new StringBuilder();
        countingBuilder.append("WITH transfer_gr as (select buyer_id, player_id, ROW_NUMBER() OVER(PARTITION BY player_id ORDER BY date DESC) AS t_num FROM transfers)");
        countingBuilder.append(" SELECT COUNT(*) from football_player p");
        countingBuilder.append(" LEFT JOIN transfer_gr t on t.player_id = p.id LEFT JOIN club c on c.id = t.buyer_id  WHERE (t.t_num = 1 OR t.t_num is NULL) AND p.active = true");

        StringBuilder whereBuilder = new StringBuilder(" AND (LOWER(concat(p.name, ' ', p.last_name)) like :key OR LOWER(concat(p.last_name, ' ', p.name)) like :key)");

        MapSqlParameterSource namedParameters
                = new MapSqlParameterSource("key", keyword);

        if (clubIds != null && !clubIds.isEmpty()) {
            whereBuilder.append(" AND c.id in (:clubIds )");
            namedParameters.addValue("clubIds", clubIds);
        }
        if (countries != null && !countries.isEmpty()) {
            whereBuilder.append(" AND p.country in (:countries)");
            namedParameters.addValue("countries", countries);
        }
        if (isPlayerId) {
            whereBuilder.append(" AND p.id = :playerId");
            namedParameters.addValue("playerId", playerId);
        }
        if (positions != null && !positions.isEmpty()) {
            whereBuilder.append(" AND p.position in (:positions)");
            namedParameters.addValue("positions", positions);
        }

        stringBuilder.append(whereBuilder.toString());
        countingBuilder.append(whereBuilder.toString());
        if (!isPlayerId) {
            stringBuilder.append(paginator.orderByClause());
        }

        List<FootballPlayer> result = namedParameterJdbcTemplate.query(
                stringBuilder.toString(),
                namedParameters,
                new FootballPlayerMapper(isPlayerId));

        Long count = !isPlayerId
                ? namedParameterJdbcTemplate.queryForObject(countingBuilder.toString(), namedParameters, Long.class)
                : null;

        return ResponseEntity.ok(new TableResponse<>(count, result));
    }

    @SuppressWarnings("null")
    public ResponseEntity<FootballPlayer> findPlayerById(Integer id) {
        ResponseEntity<TableResponse<FootballPlayer>> response = findAll(new Paginator(1, 1, false, "id"),
                null, null, null, null, null, id);
        if (response.getBody() == null || response.getBody().getContent() == null || response.getBody().getContent().isEmpty()) {
            return ResponseEntity.notFound().build();
        }
        List<FootballPlayer> result = (List<FootballPlayer>) response.getBody().getContent();
        return ResponseEntity.ok().body(result.get(0));
    }

    @Transactional
    public ResponseEntity<?> save(FootballPlayer model) {
        Club club = null;
        if (model.getClub() != null) {
            club = clubRepository.findById(model.getClub().getId()).get();
            if (club == null) {
                return ResponseEntity.notFound().build();
            }
        }
        Map<String, String> map = errors(model);
        if (map == null) {
            FootballPlayer savedPlayer = playerRepository.save(model);
            if (club != null) {
                final Transfer transfer = new Transfer(null, club, savedPlayer, 0);
                transferRepository.save(transfer);
            }
            return ResponseEntity.ok().build();
        }
        return ResponseEntity.badRequest().body(map);
    }

    public ResponseEntity<FootballManager> changeManager(Integer id, Integer managerId) {
        return managerRepository.findByPrimaryKey(managerId).map(manager -> {
            playerRepository.setManager(id, managerId);
            return ResponseEntity.ok(manager);
        }).orElse(ResponseEntity.notFound().build());
    }

    public ResponseEntity<Integer> getClubIdByPlayerId(Integer id) {
        Integer clubId = transferRepository.getClubIdByPlayerId(id);
        return ResponseEntity.ok(clubId);
    }

}
