package com.example.facebookexample.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.example.facebookexample.model.Game;

@Repository
public interface GamesRepository extends JpaRepository<Game, Integer>, JpaSpecificationExecutor<Game> {

    @Modifying
    @Transactional
    @Query("UPDATE Game g set g.homeGoals = ?1, g.guestGoals = ?2 WHERE g.id = ?3")
    int changeResult(byte homeGoals, byte guestGoals, int gameId);

    @Query(value = "SELECT distinct(YEAR(g.date)) as years FROM game g order by years desc", nativeQuery = true)
    List<Integer> years();

    @Query(value = "SELECT COUNT(*) FROM game WHERE h_id = ?1 AND g_id = ?2 AND YEAR(date) = ?3", nativeQuery = true)
    int findByHomeIdAndGustIdAndYear(Integer homeId, Integer guestId, Integer year);

}
