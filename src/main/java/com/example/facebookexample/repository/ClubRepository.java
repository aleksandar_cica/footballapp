package com.example.facebookexample.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.example.facebookexample.model.Club;

@Repository
public interface ClubRepository extends JpaRepository<Club, Integer>, JpaSpecificationExecutor<Club> {

    @Query(name = "findByName", nativeQuery = true)
    List<Club> findByName(String keyword, Integer limit, Integer offset);

    @SuppressWarnings("null")
    @Query(name = "findClubById", nativeQuery = true)
    @Override
    Optional<Club> findById(Integer id);

}
