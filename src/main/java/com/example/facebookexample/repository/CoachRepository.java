package com.example.facebookexample.repository;

import java.util.Optional;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.example.facebookexample.model.Coach;

@Repository
public interface CoachRepository extends PersonRepository<Coach> {

    @Modifying
    @Transactional
    @Query(value = "UPDATE coach set club_id = null WHERE club_id = ?1", nativeQuery = true)
    int releaseCoach(Integer clubId);

    @Query(nativeQuery = true, name = "findById")
    Optional<Coach> findByPrimaryKey(Integer id);
}
