package com.example.facebookexample.repository;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.transaction.annotation.Transactional;

import com.example.facebookexample.model.Person;

@NoRepositoryBean
public interface PersonRepository<T extends Person> extends JpaRepository<T, Integer>, JpaSpecificationExecutor<T> {

    Optional<T> findByActiveTrueAndId(Integer id);

    @Query("SELECT t from #{#entityName} t WHERE active = true AND LOWER(CONCAT(t.name, ' ',t.lastName)) LIKE LOWER(CONCAT('%', ?1,'%'))")
    Page<T> findByNameOrLastNameWithPagination(String key, PageRequest pageRequest);

    @Modifying
    @Transactional
    @Query("UPDATE #{#entityName} u set u.active = false WHERE id = ?1")
    int deactivate(Integer id);
}
