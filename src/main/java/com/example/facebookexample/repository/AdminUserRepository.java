package com.example.facebookexample.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.example.facebookexample.model.AdminUser;

@Repository
public interface AdminUserRepository extends JpaRepository<AdminUser, Integer> {

    @Modifying
    @Transactional
    @Query(value = "DELETE from user_roles WHERE admin_user_id = ?1", nativeQuery = true)
    int deleteUserRolesByUserId(Integer adminUserId);

    @Modifying
    @Transactional
    @Query(value = "UPDATE admin_user set password = ?2 WHERE id = ?1", nativeQuery = true)
    int changePassword(Integer adminUserId, String password);

    @Modifying
    @Transactional
    @Query(value = "UPDATE admin_user set token = ?2 WHERE id = ?1", nativeQuery = true)
    int changeToken(Integer adminUserId, String token);

    @Modifying
    @Transactional
    @Query(value = "UPDATE admin_user set tables_configuration = cast(?2 as JSON) WHERE id = ?1", nativeQuery = true)
    int updateTablesConfiguration(Integer adminUserId, String tablesConfiguration);

    @Modifying
    @Transactional
    @Query(value = "INSERT INTO user_roles (admin_user_id, role_id) values (?1, ?2) ", nativeQuery = true)
    void insertUserRole(Integer adminUserId, Integer roleId);

    @Modifying
    @Transactional
    @Query(value = "UPDATE admin_user set image = ?2 WHERE id = ?1", nativeQuery = true)
    int updateImage(Integer id, byte[] image);

    @Query(value = "SELECT image from admin_user WHERE id = ?1", nativeQuery = true)
    byte[] getImageById(Integer id);
}
