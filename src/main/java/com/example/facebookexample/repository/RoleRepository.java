package com.example.facebookexample.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.example.facebookexample.model.Role;

@Repository
public interface RoleRepository extends JpaRepository<Role, Integer> {

    @SuppressWarnings("null")
    @Query(name = "findAllRoles", nativeQuery = true)
    @Override
    List<Role> findAll();
}
