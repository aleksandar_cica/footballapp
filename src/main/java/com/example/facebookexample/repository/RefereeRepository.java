package com.example.facebookexample.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import com.example.facebookexample.model.MainReferee;

@Repository
public interface RefereeRepository extends JpaRepository<MainReferee, Integer>, JpaSpecificationExecutor<MainReferee> {

    Optional<MainReferee> findByActiveTrueAndId(Integer id);

}
