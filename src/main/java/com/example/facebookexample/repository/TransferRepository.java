package com.example.facebookexample.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.example.facebookexample.model.Transfer;

@Repository
public interface TransferRepository extends JpaRepository<Transfer, Integer>, JpaSpecificationExecutor<Transfer> {

    @Query(value = "SELECT buyer_id from transfers where player_id = ?1 order by date desc limit 1", nativeQuery = true)
    Integer getClubIdByPlayerId(Integer playerId);
}
