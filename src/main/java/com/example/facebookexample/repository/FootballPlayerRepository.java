package com.example.facebookexample.repository;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.example.facebookexample.model.FootballPlayer;

@Repository
public interface FootballPlayerRepository extends PersonRepository<FootballPlayer> {

    @Modifying
    @Transactional
    @Query(value = "UPDATE football_player set manager = ?2 where id = ?1", nativeQuery = true)
    public int setManager(Integer id, Integer managerId);

}
