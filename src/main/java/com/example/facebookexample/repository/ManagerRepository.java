package com.example.facebookexample.repository;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.example.facebookexample.model.FootballManager;

@Repository
public interface ManagerRepository extends PersonRepository<FootballManager> {

    @Query(nativeQuery = true, name = "findByPrimaryKey")
    Optional<FootballManager> findByPrimaryKey(Integer primaryKey);

    @Query(value = "SELECT id, name, last_name, country, active FROM football_manager WHERE active = true AND (LOWER(concat(name, ' ',last_name)) like :keyword OR LOWER(concat(last_name, ' ',name)) like :keyword)",
            countQuery = "SELECT count(*) FROM Users WHERE active = true AND (LOWER(concat(name, ' ',last_name)) like :keyword OR LOWER(concat(last_name, ' ',name)) like :keyword)",
            nativeQuery = true)
    Page<FootballManager> findByNameOrLastNameLike(@Param("keyword") String keyword, Pageable pageable);

}
