package com.example.facebookexample.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface DeleteModification {

    String field() default "active";

    String deleteValue() default "false";

    boolean delete() default false;

}
