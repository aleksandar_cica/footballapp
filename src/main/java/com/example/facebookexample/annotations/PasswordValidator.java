package com.example.facebookexample.annotations;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class PasswordValidator implements ConstraintValidator<Password, String> {

    @Override
    public boolean isValid(String arg0, ConstraintValidatorContext arg1) {
        if (arg0 == null || arg0.length() < 3) {
            return false;
        }
        return arg0.chars().anyMatch(Character::isSpaceChar) && arg0.chars().anyMatch(Character::isUpperCase) && arg0.chars().anyMatch(Character::isDigit) && arg0.chars().anyMatch(Character::isLowerCase);
    }

}
