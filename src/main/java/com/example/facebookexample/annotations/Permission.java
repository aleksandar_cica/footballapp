package com.example.facebookexample.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import com.example.facebookexample.model.PermissionsEnum;

@Target({ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
public @interface Permission {

    PermissionsEnum permission() default PermissionsEnum.NO_RESTRICTIONS;
}
