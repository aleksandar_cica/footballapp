package com.example.facebookexample.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.facebookexample.annotations.Permission;
import com.example.facebookexample.dtoModel.AddressDto;
import com.example.facebookexample.model.AdminUser;
import com.example.facebookexample.model.PermissionsEnum;
import com.example.facebookexample.service.AddressService;

@RestController
@RequestMapping("/address")
public class AddressController {

    @Autowired
    private AddressService service;

    @GetMapping("/countries")
    public ResponseEntity<List<AddressDto>> countries(@Permission(permission = PermissionsEnum.NO_RESTRICTIONS) AdminUser user) {
        return service.countries();
    }

    @GetMapping("/cities")
    public ResponseEntity<List<AddressDto>> cities(@Permission(permission = PermissionsEnum.NO_RESTRICTIONS) AdminUser user) {
        return service.cities();
    }

}
