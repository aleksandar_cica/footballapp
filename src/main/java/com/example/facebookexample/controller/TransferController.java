package com.example.facebookexample.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.facebookexample.annotations.DateRangeParam;
import com.example.facebookexample.annotations.Permission;
import com.example.facebookexample.dtoModel.TableResponse;
import com.example.facebookexample.model.AdminUser;
import com.example.facebookexample.model.PermissionsEnum;
import com.example.facebookexample.model.Transfer;
import com.example.facebookexample.service.TransferService;
import com.example.facebookexample.utilities.DateRange;
import com.example.facebookexample.utilities.Paginator;

@RestController
@RequestMapping("/transfers")
public class TransferController {

    @Autowired
    private TransferService service;

    @GetMapping()
    public ResponseEntity<TableResponse<Transfer>> findAll(
            @Permission(permission = PermissionsEnum.VIEW_TRANSFERS) AdminUser user,
            Paginator paginator,
            @DateRangeParam(name = "date") DateRange dateRange,
            @RequestParam(name = "clubId", required = false) Integer clubId,
            @RequestParam(name = "playerId", required = false) Integer playerId,
            @RequestParam(name = "onlyActivePlayers", required = false) Boolean onlyActivePlayers,
            @RequestParam(name = "name", required = false) String playerName) {
        return service.findAll(paginator, clubId, playerId, onlyActivePlayers, playerName, dateRange);
    }

    @PostMapping()
    public ResponseEntity<?> save(@Permission(permission = PermissionsEnum.MANAGE_TRANSFERS) AdminUser user, @RequestBody Transfer transfer) {
        return service.save(transfer);
    }
}
