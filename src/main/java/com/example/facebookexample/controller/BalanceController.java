package com.example.facebookexample.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.facebookexample.annotations.Permission;
import com.example.facebookexample.dtoModel.ClubTransfersBalance;
import com.example.facebookexample.model.AdminUser;
import com.example.facebookexample.model.PermissionsEnum;
import com.example.facebookexample.service.TransferService;
import com.example.facebookexample.utilities.Paginator;

@RestController
@RequestMapping("/balance")
public class BalanceController {

    @Autowired
    private TransferService service;

    @GetMapping()
    public ResponseEntity<List<ClubTransfersBalance>> transferBalance(
            @Permission(permission = PermissionsEnum.VIEW_TRANSFERS) AdminUser user,
            Paginator paginator,
            @RequestParam(name = "season", required = false) Integer season) {
        return service.transferBalanceBySeason(paginator, season);
    }

}
