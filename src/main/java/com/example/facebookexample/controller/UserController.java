package com.example.facebookexample.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.facebookexample.annotations.Permission;
import com.example.facebookexample.dtoModel.TableResponse;
import com.example.facebookexample.model.AdminUser;
import com.example.facebookexample.model.PermissionsEnum;
import com.example.facebookexample.model.User;
import com.example.facebookexample.service.UserService;
import com.example.facebookexample.utilities.Paginator;

@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserService service;

    @GetMapping("/current")
    public ResponseEntity<AdminUser> currentUser(AdminUser adminUser) {
        return ResponseEntity.ok(adminUser);
    }

    @GetMapping()
    public ResponseEntity<TableResponse<AdminUser>> findAll(
            @Permission(permission = PermissionsEnum.VIEW_USERS) AdminUser user,
            Paginator paginator,
            @RequestParam(defaultValue = "", name = "name", required = false) String username) {
        return service.findAll(paginator, username);
    }

    @PostMapping
    public ResponseEntity<?> save(
            @Permission(permission = PermissionsEnum.MANAGE_USERS) AdminUser adminUser,
            @RequestParam(defaultValue = "user", name = "type", required = false) String type,
            @RequestBody User user) {
        return service.save(user, type);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> delete(@Permission(permission = PermissionsEnum.MANAGE_USERS) AdminUser adminUser,
            @PathVariable("id") Integer id) {
        return service.deleteById(id);
    }

    @GetMapping("/{id}")
    public ResponseEntity<AdminUser> findById(@Permission(permission = PermissionsEnum.VIEW_USERS) AdminUser adminUser,
            @PathVariable("id") Integer id) {
        return service.findById(id);
    }

    @PutMapping("/image")
    public ResponseEntity<?> updateImage(AdminUser adminUser, @RequestBody String image) {
        return service.updateImage(adminUser.getId(), image);
    }

    @PutMapping("/tables-configuration")
    public ResponseEntity<?> updateTablesConfiguration(@Permission(permission = PermissionsEnum.MANAGE_USERS) AdminUser adminUser, @RequestBody String tableConfigurations) {
        return service.updateTablesConfiguration(adminUser.getId(), tableConfigurations);
    }

    @PutMapping("/resetImage")
    public ResponseEntity<?> resetImage(@Permission(permission = PermissionsEnum.MANAGE_USERS) AdminUser adminUser) {
        return service.updateImage(adminUser.getId(), null);
    }

    @PutMapping("/updateRoles/{id}")
    public ResponseEntity<?> updateRoles(@Permission(permission = PermissionsEnum.MANAGE_USERS) AdminUser adminUser, @PathVariable("id") Integer id, @RequestBody List<Integer> roles) {
        return service.updateRoles(id, roles);
    }

    @GetMapping("/image")
    public ResponseEntity<?> getImage(@Permission(permission = PermissionsEnum.NO_RESTRICTIONS) AdminUser adminUser) {
        return service.getImage(adminUser.getId());
    }

    @GetMapping("/image/{id}")
    public ResponseEntity<?> getImageById(@Permission(permission = PermissionsEnum.NO_RESTRICTIONS) AdminUser adminUser, @PathVariable("id") Integer id) {
        return service.getImage(id);
    }
}
