package com.example.facebookexample.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ConfigurationController {

    @GetMapping("/configuration/{className}")
    public ResponseEntity<?> update(@PathVariable("className") String className) {
        // String name = className.equals("referee") ? "mainReferee" : className.equals("player") ? "footballPlayer" : className;
        // List<FieldConfiguration> list = ClassConfiguration.getModelConfiguration(name);
        // if (list == null) {
        //     return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        // }
        return ResponseEntity.ok().build();
    }

}
