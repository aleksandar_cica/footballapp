package com.example.facebookexample.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.facebookexample.annotations.Permission;
import com.example.facebookexample.dtoModel.PersonDto;
import com.example.facebookexample.dtoModel.TableResponse;
import com.example.facebookexample.model.AdminUser;
import com.example.facebookexample.model.FootballManager;
import com.example.facebookexample.model.FootballPlayer;
import com.example.facebookexample.model.PermissionsEnum;
import com.example.facebookexample.service.FootballPlayerService;
import com.example.facebookexample.utilities.Paginator;

@RestController
@RequestMapping("/players")
public class FootballPlayerController {

    @Autowired
    private FootballPlayerService service;

    @GetMapping()
    public ResponseEntity<TableResponse<FootballPlayer>> findAll(
            @Permission(permission = PermissionsEnum.VIEW_PLAYERS) AdminUser adminUser,
            Paginator paginator,
            @RequestParam(defaultValue = "", name = "name", required = false) String name,
            @RequestParam(name = "countries", required = false) List<String> countries,
            @RequestParam(name = "managerId", required = false) Integer managerId,
            @RequestParam(name = "clubIds", required = false) List<Integer> clubIds,
            @RequestParam(name = "positions", required = false) List<String> positions) {
        return service.findAll(paginator, name, countries, clubIds, positions, managerId, null);
    }

    @GetMapping("/select")
    public ResponseEntity<List<PersonDto>> getPlayerForSelect(
            @RequestParam(name = "offset", required = true) int offset,
            @RequestParam(name = "limit", required = true) int limit,
            @RequestParam(name = "keyword", required = true) String keyword) {
        return service.getPlayerForSelect(keyword, limit, offset);
    }

    @PostMapping()
    public ResponseEntity<?> save(@Permission(permission = PermissionsEnum.MANAGE_PLAYERS) AdminUser adminUser, @RequestBody FootballPlayer model) {
        return service.save(model);
    }

    @PutMapping("/{id}")
    public ResponseEntity<?> update(@Permission(permission = PermissionsEnum.MANAGE_PLAYERS) AdminUser adminUser, @PathVariable("id") Integer id, @RequestBody FootballPlayer model) {
        return service.updateEntity(model, id);
    }

    @PutMapping("/{id}/changeManager/{managerId}")
    public ResponseEntity<FootballManager> changeManager(@Permission(permission = PermissionsEnum.MANAGE_PLAYERS) AdminUser adminUser, @PathVariable("id") Integer id, @PathVariable("managerId") Integer managerId) {
        return service.changeManager(id, managerId);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> delete(@Permission(permission = PermissionsEnum.MANAGE_PLAYERS) AdminUser adminUser, @PathVariable("id") Integer id) {
        return service.deleteById(id);
    }

    @GetMapping("/{id}")
    public ResponseEntity<FootballPlayer> findPlayerById(
            @Permission(permission = PermissionsEnum.VIEW_PLAYERS) AdminUser adminUser,
            @PathVariable("id") Integer id) {
        return service.findPlayerById(id);
    }

    @GetMapping("/clubId/{id}")
    public ResponseEntity<Integer> getClubIdByPlayerId(
            @Permission(permission = PermissionsEnum.VIEW_PLAYERS) AdminUser adminUser,
            @PathVariable("id") Integer id) {
        return service.getClubIdByPlayerId(id);
    }

}
