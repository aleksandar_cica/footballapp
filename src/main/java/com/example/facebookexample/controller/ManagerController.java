package com.example.facebookexample.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.facebookexample.annotations.Permission;
import com.example.facebookexample.dtoModel.PersonDto;
import com.example.facebookexample.model.AdminUser;
import com.example.facebookexample.model.FootballManager;
import com.example.facebookexample.model.PermissionsEnum;
import com.example.facebookexample.service.ManagerService;
import com.example.facebookexample.utilities.Paginator;

@RestController
@RequestMapping("/manager")
public class ManagerController {

    @Autowired
    private ManagerService service;

    @GetMapping
    public ResponseEntity<Page<FootballManager>> findAll(
            @Permission(permission = PermissionsEnum.VIEW_MANAGERS) AdminUser adminUser,
            Paginator paginator,
            @RequestParam(defaultValue = "", name = "name", required = false) String name) {
        return service.findAll(paginator, name);
    }

    @GetMapping("/select")
    public ResponseEntity<List<PersonDto>> select(
            @RequestParam(name = "offset", required = true) int offset,
            @RequestParam(name = "limit", required = true) int limit,
            @RequestParam(name = "keyword", required = true) String keyword) {
        return service.select(limit, offset, keyword);
    }

    @PostMapping()
    public ResponseEntity<?> save(@Permission(permission = PermissionsEnum.MANAGE_MANAGERS) AdminUser adminUser, @RequestBody FootballManager manager) {
        return service.saveEntity(manager);
    }

    @PutMapping("/{id}")
    public ResponseEntity<?> update(@Permission(permission = PermissionsEnum.MANAGE_MANAGERS) AdminUser adminUser,
            @PathVariable("id") Integer id, @RequestBody FootballManager manager) {
        return service.updateEntity(manager, id);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> delete(@Permission(permission = PermissionsEnum.MANAGE_MANAGERS) AdminUser adminUser, @PathVariable("id") Integer id) {
        return service.deleteById(id);
    }

}
