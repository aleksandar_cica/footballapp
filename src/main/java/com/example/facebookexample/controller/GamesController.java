package com.example.facebookexample.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.facebookexample.annotations.DateRangeParam;
import com.example.facebookexample.annotations.Permission;
import com.example.facebookexample.dtoModel.CubResultDto;
import com.example.facebookexample.dtoModel.TableResponse;
import com.example.facebookexample.model.AdminUser;
import com.example.facebookexample.model.Game;
import com.example.facebookexample.model.PermissionsEnum;
import com.example.facebookexample.service.GameService;
import com.example.facebookexample.utilities.DateRange;
import com.example.facebookexample.utilities.Paginator;

@RestController
@RequestMapping("/games")
public class GamesController {

    @Autowired
    private GameService service;

    @PostMapping
    public ResponseEntity<?> save(@Permission(permission = PermissionsEnum.MANAGE_GAMES) AdminUser adminUser, @RequestBody Game game) {
        return service.save(game);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Integer> update(@Permission(permission = PermissionsEnum.MANAGE_GAMES) AdminUser adminUser, @PathVariable("id") Integer id, @RequestBody Game game) {
        return service.update(id, game);
    }

    @GetMapping("/years")
    public ResponseEntity<List<Integer>> years() {
        return service.years();
    }

    @GetMapping
    public ResponseEntity<TableResponse<Game>> findAll(Paginator paginator,
            @DateRangeParam(name = "date") DateRange dateRange,
            @RequestParam(name = "refereeId", required = false) Integer refereeId,
            @RequestParam(name = "clubId", required = false) Integer clubId) {
        return service.findAll(paginator, dateRange, clubId, refereeId);
    }

    @GetMapping("/statistics/{season}")
    public ResponseEntity<List<CubResultDto>> goalsStatistics(@PathVariable("season") Integer season) {
        return service.goalsStatistics(season);
    }

    @GetMapping("/find")
    public ResponseEntity<Integer> findByHomeIdAndGustIdAndYear(
            @RequestParam(name = "homeId", required = true) int homeId,
            @RequestParam(name = "guestId", required = true) int guestId,
            @RequestParam(name = "year", required = true) int year) {
        return service.findByHomeIdAndGustIdAndYear(homeId, guestId, year);
    }

}
