package com.example.facebookexample.controller;

import java.util.Map;
import java.util.TreeSet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.facebookexample.annotations.Permission;
import com.example.facebookexample.model.AdminUser;
import com.example.facebookexample.model.PermissionsEnum;
import com.example.facebookexample.model.Role;
import com.example.facebookexample.service.RolesService;

@RestController
@RequestMapping("/roles")
public class RolesController {

    @Autowired
    private RolesService roleService;

    @GetMapping
    public ResponseEntity<Map<String, TreeSet<Role>>> findAll(@Permission(permission = PermissionsEnum.NO_RESTRICTIONS) AdminUser user) {
        return roleService.findAll();
    }
}
