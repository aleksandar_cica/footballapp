package com.example.facebookexample.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.facebookexample.annotations.Permission;
import com.example.facebookexample.dtoModel.CubResultDto;
import com.example.facebookexample.model.AdminUser;
import com.example.facebookexample.model.Club;
import com.example.facebookexample.model.PermissionsEnum;
import com.example.facebookexample.service.ClubService;

@RestController
@RequestMapping("/clubs")
public class ClubController {

    @Autowired
    private ClubService service;

    @GetMapping()
    public ResponseEntity<List<CubResultDto>> findBySeason(
            @Permission(permission = PermissionsEnum.NO_RESTRICTIONS) AdminUser user,
            @RequestParam("season") Integer season,
            @RequestParam(defaultValue = "points", name = "column", required = false) String column,
            @RequestParam(defaultValue = "true", name = "reverse", required = false) boolean reverse) {
        return service.findBySeason(season, column, reverse);
    }

    @GetMapping("/clubNames")
    public ResponseEntity<List<Club>> select(@Permission(permission = PermissionsEnum.NO_RESTRICTIONS) AdminUser user) {
        return service.getAllClubsNames();
    }

    @GetMapping("/find/{id}")
    public ResponseEntity<Club> find(@Permission(permission = PermissionsEnum.NO_RESTRICTIONS) AdminUser user, @PathVariable("id") Integer id) {
        return service.find(id);
    }
}
