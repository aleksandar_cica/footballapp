package com.example.facebookexample.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.facebookexample.annotations.Permission;
import com.example.facebookexample.dtoModel.TableResponse;
import com.example.facebookexample.model.AdminUser;
import com.example.facebookexample.model.MainReferee;
import com.example.facebookexample.model.PermissionsEnum;
import com.example.facebookexample.service.RefereeService;
import com.example.facebookexample.utilities.Paginator;

@RestController
@RequestMapping("/referees")
public class RefereeController {

    @Autowired
    private RefereeService service;

    @GetMapping
    public ResponseEntity<TableResponse<MainReferee>> findAll(
            @Permission(permission = PermissionsEnum.VIEW_REFEREES) AdminUser user, Paginator paginator,
            @RequestParam(name = "towns", required = false) List<String> towns,
            @RequestParam(defaultValue = "", name = "name", required = false) String name) {
        return service.findAll(paginator, name, towns);
    }

    @GetMapping("/select")
    public ResponseEntity<List<MainReferee>> select(
            @RequestParam(name = "limit", required = true) int limit,
            @RequestParam(name = "offset", required = true) int offset,
            @RequestParam(name = "keyword", required = true) String keyword) {
        return service.select(keyword, limit, offset);
    }

    @PostMapping
    public ResponseEntity<?> save(@Permission(permission = PermissionsEnum.MANAGE_REFEREES) AdminUser user, @RequestBody MainReferee referee) {
        return service.saveEntity(referee);
    }

    @PutMapping("/{id}")
    public ResponseEntity<?> update(@Permission(permission = PermissionsEnum.MANAGE_REFEREES) AdminUser user, @PathVariable("id") Integer id, @RequestBody MainReferee referee) {
        return service.updateEntity(referee, id);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> delete(@Permission(permission = PermissionsEnum.MANAGE_REFEREES) AdminUser user, @PathVariable("id") Integer id) {
        return service.deleteById(id);
    }

}
