package com.example.facebookexample;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import com.example.facebookexample.model.Admin;
import com.example.facebookexample.model.AdminUser;
import com.example.facebookexample.model.User;
import com.example.facebookexample.repository.AdminUserRepository;
import com.example.facebookexample.repository.ClubRepository;
import com.example.facebookexample.repository.FootballPlayerRepository;
import com.example.facebookexample.repository.GamesRepository;
import com.example.facebookexample.repository.ManagerRepository;
import com.example.facebookexample.repository.RefereeRepository;
import com.example.facebookexample.repository.RoleRepository;
import com.example.facebookexample.repository.TransferRepository;

@Component
public class InitClass {

    private static PasswordEncoder passwordEncoder;

    @Autowired
    private AdminUserRepository adminUserRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private FootballPlayerRepository footballPlayerRepository;

    @Autowired
    private RefereeRepository mainRefereeRepository;

    @Autowired
    private ManagerRepository managerRepository;

    @Autowired
    private ClubRepository clubRepository;

    @Autowired
    private GamesRepository gamesRepository;

    @Autowired
    private TransferRepository transferRepository;

    @PostConstruct
    private void init() {
        // List<AdminUser> users = List.of(
        //         saveAdminUser(false, "user1@gmail.com", "user1"),
        //         saveAdminUser(false, "user2@gmail.com", "user2"),
        //         saveAdminUser(false, "user3@gmail.com", "user3"),
        //         saveAdminUser(false, "irinacica@gmail.com", "user4"),
        //         saveAdminUser(true, "admin1@gmail.com", "admin1"),
        //         saveAdminUser(true, "admin2@gmail.com", "admin2"));

        // for (PermissionsEnum permissionsEnum : PermissionsEnum.values()) {
        //     Role role = new Role();
        //     role.setRole(permissionsEnum);
        //     role.setUsers(users.subList(0, 3));
        //     roleRepository.save(role);
        // }
        // FootballPlayer fp1 = new FootballPlayer(null, "Dusan", "Tadic", "Serbia");
        // FootballPlayer fp2 = new FootballPlayer(null, "Nemanja", "Matic", "Serbia");
        // footballPlayerRepository.save(fp1);
        // footballPlayerRepository.save(fp2);
        // FootballManager f1 = new FootballManager(null, "Pera", "Peric", "Serbia");
        // FootballManager f2 = new FootballManager(null, "Jorje", "Mendes", "Portugal");
        // FootballManager f3 = new FootballManager(null, "Mino", "Raiola", "Spain");
        // managerRepository.save(f1);
        // managerRepository.save(f2);
        // managerRepository.save(f3);
        // MainReferee mr1 = new MainReferee(null, "Martin", "Taylor", "London", null);
        // mainRefereeRepository.save(mr1);
        // MainReferee mr2 = new MainReferee(null, "Cavine", "Clarck", "Liverpool", null);
        // mainRefereeRepository.save(mr2);
        // MainReferee mr3 = new MainReferee(null, "Mark", "Clatenburg", "New Castle", null);
        // mainRefereeRepository.save(mr3);
        // String[] array = {"City", "Liverpool", "Totenham", "United", "Arsenal", "Chelsea", "Watford", "Volves",
        //     "West Ham", "Leicester", "Everton", "Bournemouth", "Newcastle", "Palace", "Brighton", "Southempton",
        //     "Burnley", "Cardiff", "Fulham", "Huddersfield"};
        // String[] cities = {"Manchester", "Liverpool", "London", "Manchester", "London", "London", "Birmingham",
        //     "WolwerHampton", "Birmingham", "Leicester", "Liverpool", "Bournemouth", "NewCastle", "Crister Palace",
        //     "Brighton", "Southempton", "Burnley", "Cardiff", "London", "Huddersfield"};
        // List<Club> list = new ArrayList<>();
        // for (int i = 0; i < 20; i++) {
        //     Club club = new Club(array[i]);
        //     club.setCity(cities[i]);
        //     club.setCoach(new Coach(null, "trener", "treneric", i % 2 == 0 ? "England" : "Spain", club));
        //     FootballPlayer fp = new FootballPlayer(null, "player" + club.getName(), "player", "England");
        //     fp.setManager(i % 3 == 0 ? f1 : i % 3 == 1 ? f2 : f3);
        //     clubRepository.save(club);
        //     footballPlayerRepository.save(fp);
        //     transferRepository.save(new Transfer(null, club, fp, 0));
        //     list.add(club);
        // }
        // for (int year = 0; year < 3; year++) {
        //     for (int i = 0; i < 20; i++) {
        //         for (int j = 0; j < 20; j++) {
        //             int hour = (int) (Math.random() * 6);
        //             if (i != j) {
        //                 LocalDateTime gameTime = LocalDateTime.of(2022 + year, 1 + (int) (Math.random() * 12),
        //                         1 + (int) (Math.random() * 28), 15 + hour, 0);
        //                 Game game = new Game(null, gameTime, list.get(i), list.get(j),
        //                         (byte) (Math.random() * 5), (byte) (Math.random() * 4), i % 3 == 0 ? mr1 : i % 3 == 1 ? mr2 : mr3);
        //                 gamesRepository.save(game);
        //             }
        //         }
        //     }
        // }
    }

    private AdminUser saveAdminUser(boolean isAdmin, String email, String password) {
        AdminUser adminUser;
        if (isAdmin) {
            adminUser = new Admin();
        } else {
            adminUser = new User();
        }
        adminUser.setEmail(email);
        adminUser.setPassword(InitClass.passwordEncoder().encode(password));
        return adminUserRepository.save(adminUser);
    }

    @Bean
    public static PasswordEncoder passwordEncoder() {
        if (InitClass.passwordEncoder == null) {
            InitClass.passwordEncoder = new BCryptPasswordEncoder();
        }
        return InitClass.passwordEncoder;
    }

}
