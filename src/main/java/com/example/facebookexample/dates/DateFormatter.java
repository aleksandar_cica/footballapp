package com.example.facebookexample.dates;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateFormatter {

    private static final SimpleDateFormat dateFormatter = new SimpleDateFormat();

    public static String format(Date date, String pattern) {
        if (date == null) {
            return "";
        }
        dateFormatter.applyPattern(pattern);
        return dateFormatter.format(date);
    }

    public static String format(String pattern) {
        dateFormatter.applyPattern(pattern);
        return dateFormatter.format(new Date());
    }

    public static Date parse(String date, String pattern) {
        dateFormatter.applyPattern(pattern);
        try {
            return dateFormatter.parse(date);
        } catch (ParseException e) {
        }
        return null;
    }

}
