package com.example.facebookexample.dates;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;

public class Dates {

    public static final int millisecondsInOneDay = 86400000;
    public static final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("EEE MMM dd yyyy HH:mm:ss 'GMT'Z");

    public static Date changeHoursMinutesAndSecondes(Date date, int hour, int minute, int seconde) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.set(Calendar.HOUR_OF_DAY, hour);
        cal.set(Calendar.MINUTE, minute);
        cal.set(Calendar.SECOND, seconde);
        return cal.getTime();
    }

    public static LocalDateTime convertStringWithTimeToLocalDateTime(String value) {
        Instant instant = Instant.parse(value);

        // Convert the Instant to a ZonedDateTime using the system default time zone
        ZonedDateTime zonedDateTime = instant.atZone(ZoneId.systemDefault());

        // Convert the ZonedDateTime to LocalDateTime
        return zonedDateTime.toLocalDateTime();
    }

    public static LocalDateTime convertStringToLocalDateTime(String value) {

        // Parse the string to a ZonedDateTime
        ZonedDateTime zonedDateTime = ZonedDateTime.parse(value.substring(0, 33), formatter);

        // Convert the ZonedDateTime to LocalDateTime
        return zonedDateTime.withZoneSameInstant(ZoneId.systemDefault()).toLocalDateTime();
    }

    public static Date addMonths(Date date, int months) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.MONTH, months);
        return cal.getTime();
    }

    public static Date addYears(Date date, int years) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.YEAR, years);
        return cal.getTime();
    }

    public static Date addDays(Date date, int days) {
        return new Date(date.getTime() + millisecondsInOneDay * days);
    }

    public static Date addHours(Date date, int hours) {
        return new Date(date.getTime() + millisecondsInOneDay / 24 * hours);
    }

    public static Date addMinutes(Date date, int minutes) {
        return new Date(date.getTime() + 60000 * minutes);
    }

    public static Date addSeconds(Date date, int seconds) {
        return new Date(date.getTime() + 1000 * seconds);
    }

    @SuppressWarnings("deprecation")
    public static int yearsDifferent(Date date, Date date1) {
        return date.getYear() - date1.getYear();
    }

    @SuppressWarnings("deprecation")
    public static int monthsDifferent(Date date, Date date1) {
        return (date.getYear() - date1.getYear()) * 12 - date.getMonth() - date.getMonth();
    }

    public static int daysDifferentNeglectHours(Date date, Date date1) {
        return (int) (date.getTime() / millisecondsInOneDay - date.getTime() / millisecondsInOneDay);
    }

    public static int daysDifferent(Date date, Date date1) {
        return (int) ((date.getTime() - date.getTime()) / millisecondsInOneDay);
    }

    public static int hoursDifferent(Date date, Date date1) {
        return (int) ((date.getTime() - date.getTime()) / 3600000);
    }

    public static int minutesDifferent(Date date, Date date1) {
        return (int) ((date.getTime() - date.getTime()) / 60000);
    }

    public static int secondsDifferent(Date date, Date date1) {
        return (int) ((date.getTime() - date.getTime()) / 1000);
    }

    public static Date firstDate(Date... dates) {
        Date firstDate = dates[0];
        for (int i = 1; i < dates.length; i++) {
            if (dates[i].before(firstDate)) {
                firstDate = dates[i];
            }
        }
        return firstDate;
    }

    public static Date lastDate(Date... dates) {
        Date lastDate = dates[0];
        for (int i = 1; i < dates.length; i++) {
            if (lastDate.before(dates[i])) {
                lastDate = dates[i];
            }
        }
        return lastDate;
    }

    public static Date addDates(Date date, Date date1) {
        return new Date(date.getTime() + date1.getTime());
    }

    public static Date subtractDates(Date date, Date date1) {
        return new Date(date.getTime() - date1.getTime());
    }

    public static Date convertLocalDateTimeToDate(LocalDateTime locDate) {
        if (locDate == null) {
            return null;
        }
        Instant instant = locDate.atZone(ZoneId.systemDefault()).toInstant();
        return Date.from(instant);
    }

    public static LocalDateTime convertDateToLocalDateTime(Date date) {
        return date == null ? null
                : Instant.ofEpochMilli(date.getTime())
                        .atZone(ZoneId.systemDefault())
                        .toLocalDateTime();
    }

}
