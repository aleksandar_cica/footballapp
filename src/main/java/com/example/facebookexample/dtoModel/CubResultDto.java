package com.example.facebookexample.dtoModel;

public class CubResultDto {

    private Integer id;
    private String name;
    private int points;
    private int scoredGoals;
    private int receivedGoals;
    private int totalGames;
    private int goalDiff;
    private double avgHomeGoals;
    private double avgGuestGoals;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }

    public int getScoredGoals() {
        return scoredGoals;
    }

    public void setScoredGoals(int scoredGoals) {
        this.scoredGoals = scoredGoals;
    }

    public int getReceivedGoals() {
        return receivedGoals;
    }

    public void setReceivedGoals(int receivedGoals) {
        this.receivedGoals = receivedGoals;
    }

    public int getTotalGames() {
        return totalGames;
    }

    public void setTotalGames(int totalGames) {
        this.totalGames = totalGames;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public int getGoalDiff() {
        return goalDiff;
    }

    public void setGoalDiff(int goalDiff) {
        this.goalDiff = goalDiff;
    }

    public void setAvgHomeGoals(double avgHomeGoals) {
        this.avgHomeGoals = avgHomeGoals;
    }

    public double getAvgHomeGoals() {
        return avgHomeGoals;
    }

    public void setAvgGuestGoals(double avgGuestGoals) {
        this.avgGuestGoals = avgGuestGoals;
    }

    public double getAvgGuestGoals() {
        return avgGuestGoals;
    }

}
