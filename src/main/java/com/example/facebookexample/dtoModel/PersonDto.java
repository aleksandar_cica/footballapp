package com.example.facebookexample.dtoModel;

public class PersonDto {

    private Integer id;
    private String name;
    private String lastName;

    public PersonDto(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    public PersonDto(Integer id, String name, String lastName) {
        this(id, name);
        this.lastName = lastName;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
}
