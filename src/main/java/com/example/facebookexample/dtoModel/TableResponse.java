package com.example.facebookexample.dtoModel;

import java.util.Collection;

public class TableResponse<T> {

    private Long totalElements;
    private Collection<T> content;

    public TableResponse() {
    }

    public TableResponse(Long totalElements, Collection<T> content) {
        this.totalElements = totalElements;
        this.content = content;
    }

    public Long getTotalElements() {
        return totalElements;
    }

    public void setTotalElements(Long totalElements) {
        this.totalElements = totalElements;
    }

    public Collection<T> getContent() {
        return content;
    }

    public void setContent(Collection<T> content) {
        this.content = content;
    }
}
