package com.example.facebookexample.dtoModel;

import com.example.facebookexample.model.Club;

public class ClubTransfersBalance {

    private Club club;
    private Long incomes, outcomes, diff;

    public ClubTransfersBalance() {
    }

    public Club getClub() {
        return club;
    }

    public void setClub(Club club) {
        this.club = club;
    }

    public Long getIncomes() {
        return incomes;
    }

    public void setIncomes(Long incomes) {
        this.incomes = incomes;
    }

    public Long getDiff() {
        return diff;
    }

    public void setDiff(Long diff) {
        this.diff = diff;
    }

    public Long getOutcomes() {
        return outcomes;
    }

    public void setOutcomes(Long outcomes) {
        this.outcomes = outcomes;
    }

}
