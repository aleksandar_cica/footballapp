package com.example.facebookexample.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Set;

import org.springframework.jdbc.core.RowMapper;

import com.example.facebookexample.dtoModel.AddressDto;

public class AddressMapper extends ResultSetProcessor implements RowMapper<AddressDto> {

    Set<String> columns;

    @Override
    public AddressDto mapRow(@SuppressWarnings("null") ResultSet rs, int rowNum) throws SQLException {
        if (columns == null) {
            columns = getColumns(rs);
        }
        AddressDto addressDto = new AddressDto();
        if (columns.contains("city")) {
            addressDto.setCity(rs.getString("city"));
        }
        if (columns.contains("country")) {
            addressDto.setCountry(rs.getString("country"));
        }
        addressDto.setEntity(rs.getString("entity"));
        return addressDto;
    }

}
