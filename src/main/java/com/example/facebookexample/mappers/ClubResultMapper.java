package com.example.facebookexample.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Set;

import org.springframework.jdbc.core.RowMapper;

import com.example.facebookexample.dtoModel.CubResultDto;

public class ClubResultMapper extends ResultSetProcessor implements RowMapper<CubResultDto> {

    Set<String> columns;

    @Override
    public CubResultDto mapRow(@SuppressWarnings("null") ResultSet rs, int rowNum) throws SQLException {
        if (columns == null) {
            columns = this.getColumns(rs);
        }
        CubResultDto c = new CubResultDto();
        c.setId(rs.getInt("id"));
        c.setName(rs.getString("name"));
        if (columns.contains("points")) {
            c.setPoints(rs.getInt("points"));
            c.setScoredGoals(rs.getInt("scoredGoals"));
            c.setReceivedGoals(rs.getInt("receivedGoals"));
            c.setTotalGames(rs.getInt("totalGames"));
            c.setGoalDiff(rs.getInt("diff"));
        } else if (columns.contains("avg_home")) {
            c.setAvgHomeGoals(rs.getDouble("avg_home"));
            c.setAvgGuestGoals(rs.getDouble("avg_guest"));
        }
        return c;
    }

}
