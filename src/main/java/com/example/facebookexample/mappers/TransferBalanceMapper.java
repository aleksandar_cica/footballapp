package com.example.facebookexample.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.example.facebookexample.dtoModel.ClubTransfersBalance;
import com.example.facebookexample.model.Club;

public class TransferBalanceMapper implements RowMapper<ClubTransfersBalance> {

    @Override
    public ClubTransfersBalance mapRow(@SuppressWarnings("null") ResultSet rs, int rowNum) throws SQLException {
        ClubTransfersBalance c = new ClubTransfersBalance();
        c.setClub(new Club(rs.getInt("clubId"), rs.getString("clubName")));
        c.setIncomes(rs.getLong("incomes"));
        c.setOutcomes(rs.getLong("outcomes"));
        c.setDiff(rs.getLong("diff"));
        return c;
    }

}
