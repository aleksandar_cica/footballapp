package com.example.facebookexample.mappers;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

public class GenericRowMapper<T> implements RowMapper<T> {

    private final Class<T> type;

    public GenericRowMapper(Class<T> type) {
        this.type = type;
    }

    @Override
    public T mapRow(@SuppressWarnings("null") ResultSet rs, int rowNum) throws SQLException {
        try {
            T instance = type.getDeclaredConstructor().newInstance();
            Field[] fields = type.getDeclaredFields();
            for (Field field : fields) {
                field.setAccessible(true);
                Object value = rs.getObject(field.getName());
                field.set(instance, value);
            }

            return instance;
        } catch (IllegalAccessException | IllegalArgumentException | InstantiationException | NoSuchMethodException | SecurityException | InvocationTargetException | SQLException e) {
            throw new SQLException("Failed to map row", e);
        }
    }
}
