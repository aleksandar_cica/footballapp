package com.example.facebookexample.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;

import org.springframework.jdbc.core.RowMapper;

import com.example.facebookexample.model.Club;
import com.example.facebookexample.model.FootballPlayer;
import com.example.facebookexample.model.Transfer;

public class TransferMapper extends ResultSetProcessor implements RowMapper<Transfer> {

    @Override
    public Transfer mapRow(@SuppressWarnings("null") ResultSet rs, int rowNum) throws SQLException {
        Transfer t = new Transfer();
        t.setTransferDate(rs.getObject("transferDate", LocalDateTime.class));
        t.setMoney(rs.getInt("money"));
        FootballPlayer player = new FootballPlayer();
        player.setId(rs.getInt("id"));
        player.setActive(rs.getBoolean("active"));
        player.setName(rs.getString("name"));
        player.setLastName(rs.getString("last_name"));
        t.setPlayer(player);
        t.setBuyerClub(new Club(rs.getObject("buyerId", Integer.class), rs.getString("buyerName")));
        Integer sailerId = this.getResultSetInteger(rs, "sailerId");
        if (sailerId != null) {
            t.setSailerClub(new Club(sailerId, rs.getString("sailerName")));
        }
        return t;
    }

}
