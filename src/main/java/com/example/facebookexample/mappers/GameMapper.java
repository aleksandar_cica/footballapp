package com.example.facebookexample.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;

import org.springframework.jdbc.core.RowMapper;

import com.example.facebookexample.model.Club;
import com.example.facebookexample.model.Game;
import com.example.facebookexample.model.MainReferee;

public class GameMapper implements RowMapper<Game> {

    @Override
    public Game mapRow(@SuppressWarnings("null") ResultSet rs, int rowNum) throws SQLException {
        Game game = new Game();
        game.setHome(new Club(rs.getObject("h_id", Integer.class), rs.getString("h_name")));
        game.setGuest(new Club(rs.getObject("g_id", Integer.class), rs.getString("g_name")));
        game.setMainReferee(new MainReferee(rs.getObject("r_id", Integer.class), rs.getString("r_name"),
                rs.getString("r_last_name"), null, null));
        game.setHomeGoals(rs.getByte("homegoals"));
        game.setGuestGoals(rs.getByte("guestgoals"));
        // LocalDateTime date = rs.getObject("date", LocalDateTime.class);
        game.setDate(rs.getObject("date", LocalDateTime.class));
        game.setId(rs.getObject("id", Integer.class));
        return game;
    }

}
