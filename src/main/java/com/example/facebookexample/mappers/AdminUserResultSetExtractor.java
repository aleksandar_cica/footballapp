package com.example.facebookexample.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.example.facebookexample.model.Admin;
import com.example.facebookexample.model.AdminUser;
import com.example.facebookexample.model.PermissionsEnum;
import com.example.facebookexample.model.Role;
import com.example.facebookexample.model.User;

public class AdminUserResultSetExtractor extends ResultSetProcessor implements ResultSetExtractor<List<AdminUser>> {

    @SuppressWarnings("null")
    @Override
    public List<AdminUser> extractData(ResultSet rs) throws SQLException, DataAccessException {
        List<AdminUser> list = new ArrayList<>();
        Set<String> columns = getColumns(rs);
        while (rs.next()) {
            AdminUser adminUser;
            if ("admin".equalsIgnoreCase(rs.getString("dtype"))) {
                adminUser = new Admin();
            } else {
                adminUser = new User();
            }
            adminUser.setId(rs.getObject("id", Integer.class));
            if (columns.contains("password")) {
                adminUser.setPassword(rs.getString("password"));
            }
            adminUser.setEmail(rs.getString("email"));
            if (columns.contains("token")) {
                adminUser.setToken(rs.getString("token"));
            }
            if (columns.contains("tables_configuration")) {
                adminUser.setTablesConfiguration(rs.getString("tables_configuration"));
            }

            Integer roleId = rs.getObject("role_id", Integer.class);
            String roleName = rs.getString("role_name");
            if (roleId != null && roleName != null) {
                Role role = new Role();
                role.setRole(PermissionsEnum.valueOf(roleName));
                role.setId(roleId);
                adminUser.getRoles().add(role);
            }
            list.add(adminUser);
        }
        List<AdminUser> transformed = list.stream()
                .collect(Collectors.groupingBy(
                        AdminUser::getId,
                        LinkedHashMap::new,
                        Collectors.reducing((p1, p2) -> {
                            p1.getRoles().addAll(p2.getRoles());
                            return p1;
                        })
                )).values().stream().map(a -> a.get())
                .filter(v -> v != null)
                .collect(Collectors.toList());
        return transformed;

    }

}
