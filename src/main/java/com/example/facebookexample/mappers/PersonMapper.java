package com.example.facebookexample.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.example.facebookexample.dtoModel.PersonDto;

public class PersonMapper implements RowMapper<PersonDto> {

    @Override
    public PersonDto mapRow(@SuppressWarnings("null") ResultSet rs, int rowNum) throws SQLException {
        return new PersonDto(
                rs.getObject("id", Integer.class),
                rs.getString("name"),
                rs.getString("last_name")
        );
    }

}
