package com.example.facebookexample.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.example.facebookexample.model.MainReferee;

public class RefereeMapper implements RowMapper<MainReferee> {

    @Override
    public MainReferee mapRow(@SuppressWarnings("null") ResultSet rs, int rowNum) throws SQLException {
        return new MainReferee(rs.getObject("id", Integer.class),
                rs.getString("name"),
                rs.getString("last_name"), rs.getString("town"),
                null);
    }

}
