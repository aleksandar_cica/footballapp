package com.example.facebookexample.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.example.facebookexample.model.Club;
import com.example.facebookexample.model.FootballManager;
import com.example.facebookexample.model.FootballPlayer;
import com.example.facebookexample.utilities.PositionDeserializer;

public class FootballPlayerMapper implements RowMapper<FootballPlayer> {

    private final boolean includeManager;

    public FootballPlayerMapper(boolean includeManager) {
        this.includeManager = includeManager;
    }

    PositionDeserializer positionDeserializer = new PositionDeserializer();

    @Override
    public FootballPlayer mapRow(@SuppressWarnings("null") ResultSet rs, int rowNum) throws SQLException {
        FootballPlayer f = new FootballPlayer();
        f.setId(rs.getInt("id"));
        f.setName(rs.getString("name"));
        f.setLastName(rs.getString("lastName"));
        f.setCountry(rs.getString("country"));
        f.setPosition(positionDeserializer.convertStringToPosition(rs.getString("position")));
        Integer clubId = rs.getObject("clubId", Integer.class);
        if (clubId != null) {
            f.setClub(new Club(clubId, rs.getString("clubName")));
        }
        if (includeManager) {
            Integer managerId = rs.getObject("managerId", Integer.class);
            boolean isManagerActive = rs.getBoolean("managerActive");
            if (managerId != null && isManagerActive) {
                f.setManager(new FootballManager(managerId,
                        rs.getString("managerName"),
                        rs.getString("managerLastName"),
                        rs.getString("managerCountry")));
            }
        }
        return f;
    }
}
