package com.example.facebookexample.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.example.facebookexample.model.Coach;

public class CoachMapper extends ResultSetProcessor implements RowMapper<Coach> {

    @Override
    public Coach mapRow(@SuppressWarnings("null") ResultSet rs, int rowNum) throws SQLException {
        return new Coach(
                rs.getObject("id", Integer.class),
                rs.getString("name"),
                rs.getString("last_name"),
                rs.getString("country"),
                getResultSetInteger(rs, "club_id"),
                rs.getString("club_name"));
    }

}
