package com.example.facebookexample.security;

import java.util.List;

import org.springframework.core.MethodParameter;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.method.support.ModelAndViewContainer;

import com.example.facebookexample.annotations.Permission;
import com.example.facebookexample.model.Admin;
import com.example.facebookexample.model.AdminUser;
import com.example.facebookexample.model.PermissionsEnum;
import com.example.facebookexample.model.Role;
import com.example.facebookexample.service.UserService;
import com.example.facebookexample.utilities.ForbiddenMethodException;

public class AdminUserParameterResolver implements HandlerMethodArgumentResolver {

    private final UserService userService;

    public AdminUserParameterResolver(UserService userService) {
        this.userService = userService;
    }

    @Override
    public boolean supportsParameter(@SuppressWarnings("null") MethodParameter parameter) {
        return parameter.getParameterType().equals(AdminUser.class)
                && parameter.getParameterAnnotation(RequestBody.class) == null;
    }

    @SuppressWarnings("null")
    @Override
    public Object resolveArgument(
            MethodParameter parameter,
            ModelAndViewContainer mavContainer,
            NativeWebRequest webRequest,
            WebDataBinderFactory binderFactory) throws ForbiddenMethodException {
        final Permission annotation = parameter.getParameterAnnotation(Permission.class);
        PermissionsEnum permission = annotation == null ? PermissionsEnum.NO_RESTRICTIONS : annotation.permission();
        webRequest.getHeader("token");
        List<AdminUser> usersList = userService.findAllUsers(null, null, false, null, webRequest.getHeader("token"));
        if (usersList == null || usersList.isEmpty()) {
            throw new ForbiddenMethodException();
        }
        boolean isAllowed = isOperationAllowed(usersList.get(0), permission);
        if (isAllowed) {
            return usersList.get(0);
        }
        throw new ForbiddenMethodException();
    }

    private boolean isOperationAllowed(AdminUser adminUser, PermissionsEnum permission) {
        List<Role> roles = adminUser.getRoles();
        return adminUser instanceof Admin || PermissionsEnum.NO_RESTRICTIONS.equals(permission)
                || (roles != null && roles.stream().filter(r -> r.getRole().equals(permission))
                        .findFirst().isPresent());
    }
}
