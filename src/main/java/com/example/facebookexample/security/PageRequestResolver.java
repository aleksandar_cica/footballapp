package com.example.facebookexample.security;

import org.springframework.core.MethodParameter;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.method.support.ModelAndViewContainer;

import com.example.facebookexample.utilities.Paginator;

public class PageRequestResolver implements HandlerMethodArgumentResolver {

    @Override
    public boolean supportsParameter(@SuppressWarnings("null") MethodParameter parameter) {
        return parameter.getParameterType().equals(Paginator.class);
    }

    @Override
    public Object resolveArgument(@SuppressWarnings("null") MethodParameter parameter, @SuppressWarnings("null") ModelAndViewContainer mavContainer,
            @SuppressWarnings("null") NativeWebRequest webRequest, @SuppressWarnings("null") WebDataBinderFactory binderFactory) throws Exception {
        String column = webRequest.getParameter("column");
        String reverse = webRequest.getParameter("reverse");
        String page = webRequest.getParameter("page");
        String pageSize = webRequest.getParameter("pageSize");
        return new Paginator(page == null ? 1 : Integer.parseInt(page), pageSize == null ? 1000 : Integer.parseInt(pageSize),
                "true".equalsIgnoreCase(reverse), column);

    }

}
