package com.example.facebookexample.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.example.facebookexample.model.AdminUser;
import com.example.facebookexample.service.UserService;

@Controller
public class SecurityController {

    @Autowired
    private UserService service;

    @PostMapping("/login")
    public ResponseEntity<?> login(@RequestBody AdminUser adminUser) {
        return service.login(adminUser);
    }

    @PostMapping("/logout")
    public ResponseEntity<?> logout(AdminUser adminUser) {
        return service.logout(adminUser);
    }

    @PutMapping("/changePassword")
    public ResponseEntity<?> changePassword(@RequestBody AdminUser adminUser) {
        return service.changePassword(adminUser);
    }
}
