package com.example.facebookexample.security;

import java.time.LocalDateTime;

import org.springframework.core.MethodParameter;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.method.support.ModelAndViewContainer;

import com.example.facebookexample.annotations.DateRangeParam;
import com.example.facebookexample.dates.Dates;
import com.example.facebookexample.utilities.DateRange;

public class DateRangeResolver implements HandlerMethodArgumentResolver {

    @Override
    public boolean supportsParameter(@SuppressWarnings("null") MethodParameter parameter) {
        return parameter.getParameterType().equals(DateRange.class);
    }

    @Override
    public Object resolveArgument(@SuppressWarnings("null") MethodParameter parameter, @SuppressWarnings("null") ModelAndViewContainer mavContainer,
            @SuppressWarnings("null") NativeWebRequest webRequest, @SuppressWarnings("null") WebDataBinderFactory binderFactory) throws Exception {
        DateRange dateRange = new DateRange();
        DateRangeParam annotation = parameter.getParameterAnnotation(DateRangeParam.class);
        if (annotation != null) {
            String name = annotation.name();
            String[] dates = webRequest.getParameterMap().get(name);

            if (dates != null && dates.length > 0) {
                LocalDateTime startDate = Dates.convertStringToLocalDateTime(dates[0]);
                if (dates.length > 1) {
                    LocalDateTime endDate = Dates.convertStringToLocalDateTime(dates[1]);
                    if (endDate.compareTo(startDate) < 0) {
                        dateRange.setStartDate(endDate);
                        dateRange.setEndDate(startDate.plusDays(1));
                    } else {
                        dateRange.setStartDate(startDate);
                        dateRange.setEndDate(endDate.plusDays(1));
                    }
                } else {
                    dateRange.setStartDate(startDate);
                }

            }
        }
        return dateRange;
    }

}
