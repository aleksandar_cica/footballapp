package com.example.facebookexample.io;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ExcelFiles {
  public static  boolean writeData(String fileName,String workSheetName,List<Map<String,Object>> data){
	  if(data==null || data.isEmpty()){
		  return false;
	  }
	  List<String> headers=new ArrayList<>();
	  Workbook workbook = new XSSFWorkbook();
	  CellStyle numberStyle = workbook.createCellStyle();
	  numberStyle.setAlignment(HorizontalAlignment.RIGHT);
	  Sheet sheet = workbook.createSheet(workSheetName);
	  Row header = sheet.createRow(0);
	  for (int i = 0; i < data.size(); i++) {
		  Row row = sheet.createRow(i+1);
		  data.get(i).entrySet().forEach(s->{
				int index=finIndex(headers, s.getKey());
				if(index==-1){
					headers.add(s.getKey());
					Cell headerCell = header.createCell(headers.size()-1);
					headerCell.setCellValue(s.getKey().toString());
					Cell cell=row.createCell(headers.size()-1);
					setType(cell, s.getValue(),numberStyle);
				}else{
					Cell cell=row.createCell(index);
					setType(cell, s.getValue(),numberStyle);
				}
			});
	 }

		
	  
	//  sheet.setColumnWidth(0, 4000);
	
//	  headerStyle.setFillForegroundColor(IndexedColors.LIGHT_BLUE.getIndex());
//	  headerStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
	   
//	  XSSFFont font = ((XSSFWorkbook) workbook).createFont();
//	  font.setFontName("Arial");
//	  font.setFontHeightInPoints((short) 16);
//	  font.setBold(true);
//	  headerStyle.setFont(font);
	  FileOutputStream outputStream;
	try {
		  outputStream = new FileOutputStream("C:\\Users\\Irina\\Downloads\\"+fileName+".xlsx");
		  workbook.write(outputStream);
		  workbook.close();
	} catch (FileNotFoundException e) {
		e.printStackTrace();
		return false;
	} catch (IOException e) {
		e.printStackTrace();
		return false;
	}
	return true;
  }
  
  private static void setType(Cell cell, Object object,CellStyle numberStyle){
	  if (object instanceof Number) {
		  cell.setCellType(CellType.NUMERIC);
		  cell.setCellStyle(numberStyle);
		  if(object instanceof Integer){
			  double value= (double) (int) object;
			  cell.setCellValue(value);
		  }else{
			  cell.setCellValue((double) object);
		  }
	  }
	  cell.setCellValue(object.toString());
  }
  
  public static<T>  Map<Integer, List<Object>> read(String path){
	  try(Workbook workbook = new XSSFWorkbook(new FileInputStream(new File(path)))) {
			Sheet sheet = workbook.getSheetAt(0);
			Map<Integer, List<Object>> data = new HashMap<Integer, List<Object>>();
			int i = 0;
			for (Row row : sheet) {
				ArrayList<Object> list = new ArrayList<Object>();
			    for (Cell cell : row) {
			        switch (cell.getCellTypeEnum()) {
			            case STRING: list.add(cell.getStringCellValue());  break;
			            case NUMERIC: list.add(cell.getNumericCellValue()); break;
			            case BOOLEAN: list.add(cell.getNumericCellValue()); break;
			            case FORMULA:  break;
			            default: data.get(new Integer(i)).add(" ");
			        }
			    }
			    data.put(i, list);
			    i++;
			}
			return data;
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	  return null;
  }
   
  public static int finIndex(List<String> list, String element){
	  for (int i = 0; i < list.size(); i++) {
		if(list.get(i).equals(element)){
			return i;
		}
	  }
	  return -1;
  }
  
}
