package com.example.facebookexample.io;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Collections;
import java.util.List;
import java.util.function.Function;

public class WebReader {

    public static String read(String source) throws IOException {
        StringBuilder stringBuilder = new StringBuilder();
        URL url = new URL(source);
        InputStream is = url.openStream();
        try (BufferedReader br = new BufferedReader(new InputStreamReader(is))) {
            String line;
            while ((line = br.readLine()) != null) {
                stringBuilder.append(line);
                stringBuilder.append(System.lineSeparator());
            }
        } catch (MalformedURLException e) {
            throw new MalformedURLException("URL is malformed!!");
        } catch (IOException e) {
            throw new IOException();
        }
        return stringBuilder.toString();
    }

    public static <T> List<T> read(String source, Function<String, T> mapFunction) throws IOException {
        List<T> list = Collections.emptyList();
        URL url = new URL(source);
        InputStream is = url.openStream();
        try (BufferedReader br = new BufferedReader(new InputStreamReader(is))) {
            String line;
            while ((line = br.readLine()) != null) {
                T element = mapFunction.apply(line);
                if (element != null) {
                    list.add(element);
                }
            }
        } catch (MalformedURLException e) {
            throw new MalformedURLException("URL is malformed!!");
        } catch (IOException e) {
            throw new IOException();
        }
        return list;
    }
}
