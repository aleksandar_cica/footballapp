package com.example.facebookexample.io;

import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.Collections;
import java.util.List;
import java.util.function.Function;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class XML {

    public static <T> List<T> readXMLFile(String fileName, String tagName, Function<Element, T> mapFunction) {
        try {
            File inputFile = new File(fileName);
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(inputFile);
            doc.getDocumentElement().normalize();
            return parseNodeList(doc.getElementsByTagName(tagName), mapFunction);
        } catch (IOException | ParserConfigurationException | SAXException e) {
            System.out.println("File " + fileName + " doesn't exist");
            return Collections.emptyList();
        }
    }

    public static <T> List<T> parseNodeList(NodeList nList, Function<Element, T> mapFunction) {
        List<T> returnValue = Collections.emptyList();
        for (int temp = 0; temp < nList.getLength(); temp++) {
            Node node = nList.item(temp);
            Element nodeElement = (Element) node;
            if (node.getNodeType() == Node.ELEMENT_NODE) {
                T element = mapFunction.apply(nodeElement);
                if (element != null) {
                    returnValue.add(element);
                }
            }
        }
        return returnValue;
    }

    public static <T> T convertXMLStringToObject(String xmlString, Class<T> cl) {
        JAXBContext jaxbContext;
        try {
            jaxbContext = JAXBContext.newInstance(cl);
            Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
            T unmarshal = (T) jaxbUnmarshaller.unmarshal(new StringReader(xmlString));
            return unmarshal;
        } catch (JAXBException e) {
        }
        return null;
    }

    public static String toXMLString(Object object) {
        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(object.getClass());
            Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
            StringWriter sw = new StringWriter();
            jaxbMarshaller.marshal(object, sw);
            return sw.toString();
        } catch (JAXBException e) {
        }
        return null;
    }
}
