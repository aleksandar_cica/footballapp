package com.example.facebookexample.io;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;
import java.util.function.Function;
import java.util.stream.Collectors;

public class Files {

    public static boolean write(String text, String path) {
        try (FileWriter myWriter = new FileWriter(path)) {
            myWriter.write(text);
            return true;
        } catch (IOException e) {
            return false;
        }
    }

    public static <T> boolean write(String path, List<T> data, Function<T, String> mapFunction) {
        String text = data.stream().filter(element -> element != null)
                .map(mapFunction).filter(element1 -> element1 != null)
                .collect(Collectors.joining(System.lineSeparator()));
        return write(text, path);
    }

    public static String read(String path) {
        StringBuilder stringBuilder = new StringBuilder();
        File file = new File(path);
        try (Scanner myReader = new Scanner(file)) {
            while (myReader.hasNextLine()) {
                stringBuilder.append(myReader.nextLine());
                stringBuilder.append(System.lineSeparator());
            }
        } catch (FileNotFoundException e) {
        }
        return stringBuilder.toString();
    }

    public static <T> List<T> read(String path, Function<String, T> mapFunction) {
        List<T> list = Collections.emptyList();
        File file = new File(path);
        try (Scanner myReader = new Scanner(file)) {
            while (myReader.hasNextLine()) {
                T element = mapFunction.apply(myReader.nextLine());
                if (element != null) {
                    list.add(element);
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return list;
    }

    public static List<String> readAllLines(String path) {
        return Arrays.asList(read(path).split(System.lineSeparator()));
    }

    public static boolean appendText(String text, String path) {
        String textFromFile = read(path);
        textFromFile += System.lineSeparator() + text;
        return write(textFromFile, path);
    }
}
