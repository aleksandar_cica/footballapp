package com.example.facebookexample.io;

import java.util.List;
import java.util.function.Function;

public abstract class DataLoader<T> {

    private List<T> data;
    private Function<String, T> parser;
    private String source;

    public DataLoader(Function<String, T> parser, String source) {
        super();
        this.parser = parser;
        this.source = source;
    }

    public DataLoader(String source) {
        super();
        this.source = source;
    }

    public DataLoader() {
    }

    public List<T> getData() {
        if (data == null) {
            data = loadData();
        }
        return data;
    }

    public void cleanData() {
        this.data = null;
    }

    public Function<String, T> getParser() {
        return parser;
    }

    public void setParser(Function<String, T> parser) {
        this.parser = parser;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public abstract List<T> loadData();

}
