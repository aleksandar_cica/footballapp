package com.example.facebookexample.io;

import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.function.Function;

public class WebLoader<T> extends DataLoader<T> {

    public WebLoader() {
        super();
    }

    public WebLoader(Function<String, T> parser, String source) {
        super(parser, source);
    }

    public WebLoader(String source) {
        super(source);
    }

    @Override
    public List<T> loadData() {
        try {
            return WebReader.read(this.getSource(), this.getParser());
        } catch (IOException e) {
            return Collections.emptyList();
        }
    }

}
