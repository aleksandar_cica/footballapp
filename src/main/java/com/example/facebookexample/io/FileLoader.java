package com.example.facebookexample.io;

import java.util.List;
import java.util.function.Function;

public class FileLoader<T> extends DataLoader<T> {

    public FileLoader() {
        super();
    }

    public FileLoader(Function<String, T> parser, String source) {
        super(parser, source);
    }

    public FileLoader(String source) {
        super(source);
    }

    @Override
    public List<T> loadData() {
        return Files.read(this.getSource(), this.getParser());
    }

}
