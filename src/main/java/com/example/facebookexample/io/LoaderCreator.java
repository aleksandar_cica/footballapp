package com.example.facebookexample.io;

import java.io.File;

public class LoaderCreator {

    public static <T> DataLoader<T> create(String source) {
        if (new File(source).exists()) {
            return new FileLoader<>(source);
        }
        return new WebLoader<>(source);
    }
}
