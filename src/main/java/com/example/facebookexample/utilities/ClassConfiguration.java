package com.example.facebookexample.utilities;

// import java.lang.reflect.Field;
// import java.lang.reflect.Modifier;
// import java.time.LocalDateTime;
// import java.util.ArrayList;
// import java.util.Date;
// import java.util.List;
// import javax.persistence.Column;
// import javax.persistence.GeneratedValue;
// import javax.persistence.JoinColumn;
// import javax.validation.constraints.Email;
// import javax.validation.constraints.Future;
// import javax.validation.constraints.Max;
// import javax.validation.constraints.Min;
// import javax.validation.constraints.NotNull;
// import javax.validation.constraints.Pattern;
// import javax.validation.constraints.Size;
// import com.example.facebookexample.annotations.NotFormField;
// import com.example.facebookexample.annotations.Password;
// import com.example.facebookexample.annotations.Select;
// import com.example.facebookexample.dtoModel.FieldConfiguration;
// import com.example.facebookexample.dtoModel.SelectConfiguration;
// import com.fasterxml.jackson.annotation.JsonIgnore;
public class ClassConfiguration {

    // public static List<FieldConfiguration> getModelConfiguration(String className) {
    //     try {
    //         Class<?> model = Class.forName("com.example.facebookexample.model."
    //                 + className.substring(0, 1).toUpperCase() + className.substring(1));
    //         List<FieldConfiguration> list = getConfiguration(model);
    //         while (!model.getSuperclass().equals(Object.class)) {
    //             model = model.getSuperclass();
    //             list.addAll(0, getConfiguration(model));
    //         }
    //         return list;
    //     } catch (ClassNotFoundException e) {
    //         e.printStackTrace();
    //     }
    //     return null;
    // }
    // public static List<FieldConfiguration> getConfiguration(Class<?> model) {
    //     List<FieldConfiguration> list = new ArrayList<>();
    //     for (Field field : model.getDeclaredFields()) {
    //         boolean hasMaxValue = false, hasMinValue = false;
    //         if (Modifier.isStatic(field.getModifiers()) || field.getAnnotation(NotFormField.class) != null
    //                 || field.getAnnotation(GeneratedValue.class) != null || field.getAnnotation(JsonIgnore.class) != null) {
    //             continue;
    //         }
    //         FieldConfiguration conf = new FieldConfiguration();
    //         conf.setName(field.getName());
    //         if (field.getAnnotation(Password.class) != null) {
    //             conf.setInput("input");
    //             conf.setType("password");
    //             conf.addConstrain("pattern", "skdldfkdl", "invalid pattern");
    //             conf.addConstrain("required", true, field.getName() + " is required");
    //         } else if (field.getAnnotation(Email.class) != null) {
    //             conf.setInput("input");
    //             conf.setType("email");
    //             conf.addConstrain("pattern", "skdldfkdl", "email should have @");
    //             if (field.getAnnotation(NotNull.class) != null) {
    //                 conf.addConstrain("required", true, field.getName() + " is required");
    //             }
    //         } else if (field.getType().isEnum() || field.getAnnotation(Select.class) != null) {
    //             SelectConfiguration selectConf = new SelectConfiguration();
    //             selectConf.setInput("select");
    //             selectConf.setName(field.getName());
    //             if (field.getType().isEnum()) {
    //                 selectConf.setData(field.getType().getEnumConstants());
    //             } else {
    //                 Select select = field.getAnnotation(Select.class);
    //                 selectConf.setLabels(select.labels().split(" "));
    //                 selectConf.setId(select.id());
    //                 selectConf.setUrl(select.url());
    //                 selectConf.setMultiple(select.multiple());
    //                 Column column = field.getAnnotation(Column.class);
    //                 if (column != null && !column.nullable()) {
    //                     selectConf.addConstrain("required", true, field.getName() + " is required");
    //                 }
    //                 if (select.required()) {
    //                     selectConf.addConstrain("required", true, field.getName() + " is required");
    //                 } else {
    //                     JoinColumn joinColumn = field.getAnnotation(JoinColumn.class);
    //                     if (joinColumn != null && !joinColumn.nullable()) {
    //                         selectConf.addConstrain("required", true, field.getName() + " is required");
    //                     }
    //                 }
    //             }
    //             list.add(selectConf);
    //             continue;
    //         } else {
    //             Size size = field.getAnnotation(Size.class);
    //             if (size != null) {
    //                 conf.addConstrain("minlenght", size.min(), size.message());
    //                 conf.addConstrain("maxlenght", size.max(), size.message());
    //             }
    //             NotNull notNull = field.getAnnotation(NotNull.class);
    //             if (notNull != null) {
    //                 conf.addConstrain("required", true, field.getName() + " is required");
    //             }
    //             Column column = field.getAnnotation(Column.class);
    //             if (column != null && !column.nullable()) {
    //                 conf.addConstrain("required", true, field.getName() + " is required");
    //             }
    //             Min min = field.getAnnotation(Min.class);
    //             if (min != null) {
    //                 hasMinValue = true;
    //                 conf.addConstrain("min", min.value(), min.message());
    //             }
    //             Max max = field.getAnnotation(Max.class);
    //             if (max != null) {
    //                 hasMaxValue = true;
    //                 conf.addConstrain("max", max.value(), max.message());
    //             }
    //             Pattern pattern = field.getAnnotation(Pattern.class);
    //             if (pattern != null) {
    //                 conf.addConstrain("pattern", pattern.regexp(), pattern.message());
    //             }
    //             Future future = field.getAnnotation(Future.class);
    //             if (future != null) {
    //                 conf.addConstrain("future", null, future.message());
    //             }
    //             JoinColumn joinColumn = field.getAnnotation(JoinColumn.class);
    //             if (joinColumn != null && !joinColumn.nullable()) {
    //                 conf.addConstrain("required", true, field.getName() + " is required");
    //             }
    //             if (field.getType().isPrimitive()) {
    //                 switch (field.getType().getName().toLowerCase()) {
    //                     case "byte":
    //                         if (!hasMaxValue) {
    //                             conf.addConstrain("max", Byte.MAX_VALUE, "max value is " + Byte.MAX_VALUE);
    //                         }
    //                         if (!hasMinValue) {
    //                             conf.addConstrain("min", Byte.MIN_VALUE, "min value is " + Byte.MIN_VALUE);
    //                         }
    //                         conf.setType("number");
    //                         break;
    //                     case "short":
    //                         if (!hasMaxValue) {
    //                             conf.addConstrain("max", Short.MAX_VALUE, "max value is " + Short.MAX_VALUE);
    //                         }
    //                         if (!hasMinValue) {
    //                             conf.addConstrain("min", Short.MIN_VALUE, "min value is " + Short.MIN_VALUE);
    //                         }
    //                         conf.setType("number");
    //                         break;
    //                     case "int":
    //                         if (!hasMaxValue) {
    //                             conf.addConstrain("max", Integer.MAX_VALUE, "max value is " + Integer.MAX_VALUE);
    //                         }
    //                         if (!hasMinValue) {
    //                             conf.addConstrain("min", Integer.MIN_VALUE, "min value is " + Integer.MIN_VALUE);
    //                         }
    //                         conf.setType("number");
    //                         break;
    //                     case "boolean":
    //                         conf.setType("checkbox");
    //                         break;
    //                     case "char":
    //                         if (!hasMaxValue) {
    //                             conf.addConstrain("max", Character.MAX_VALUE, "max value is " + Character.MAX_VALUE);
    //                         }
    //                         if (!hasMinValue) {
    //                             conf.addConstrain("min", Character.MIN_VALUE, "min value is " + Character.MIN_VALUE);
    //                         }
    //                         conf.setType("number");
    //                         break;
    //                     case "double":
    //                         if (!hasMaxValue) {
    //                             conf.addConstrain("max", Double.MAX_VALUE, "max value is " + Double.MAX_VALUE);
    //                         }
    //                         if (!hasMinValue) {
    //                             conf.addConstrain("min", Double.MIN_VALUE, "min value is " + Double.MIN_VALUE);
    //                         }
    //                         conf.setType("number");
    //                         break;
    //                     case "long":
    //                         if (!hasMaxValue) {
    //                             conf.addConstrain("max", Long.MAX_VALUE, "max value is " + Long.MAX_VALUE);
    //                         }
    //                         if (!hasMinValue) {
    //                             conf.addConstrain("min", Long.MIN_VALUE, "min value is " + Long.MIN_VALUE);
    //                         }
    //                         conf.setType("number");
    //                         break;
    //                     case "float":
    //                         if (!hasMaxValue) {
    //                             conf.addConstrain("max", Float.MAX_VALUE, "max value is " + Float.MAX_VALUE);
    //                         }
    //                         if (!hasMinValue) {
    //                             conf.addConstrain("min", Float.MIN_VALUE, "min value is " + Float.MIN_VALUE);
    //                         }
    //                         conf.setType("number");
    //                         break;
    //                 }
    //             } else {
    //                 if (field.getType() == String.class) {
    //                     conf.setType("string");
    //                 } else if (field.getType() == LocalDateTime.class || field.getType() == Date.class) {
    //                     conf.setInput("date");
    //                 }
    //             }
    //         }
    //         list.add(conf);
    //     }
    //     return list;
    // }
}
