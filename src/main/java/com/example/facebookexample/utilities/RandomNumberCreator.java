package com.example.facebookexample.utilities;

public class RandomNumberCreator {
	public static int randomInt(int minValue,int maxValue){
		  return (int) (minValue+(maxValue+1-minValue)*Math.random());
	}
	public static double randomDouble(int minValue,int maxValue){
		  return  minValue+(maxValue-minValue)*Math.random();
	}
}
