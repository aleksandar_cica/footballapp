package com.example.facebookexample.utilities;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;

public class Paginator {

    private int page, pageSize;
    private boolean reverse;
    private String column;

    public Paginator(int page, int pageSize) {
        this.page = page;
        this.pageSize = pageSize;
    }

    public Paginator(int page, int pageSize, boolean reverse, String column) {
        this.page = page;
        this.pageSize = pageSize;
        this.reverse = reverse;
        this.column = column;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public String getColumn() {
        return column;
    }

    public void setColumn(String column) {
        this.column = column;
    }

    public boolean isReverse() {
        return reverse;
    }

    public void setReverse(boolean reverse) {
        this.reverse = reverse;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getOffset() {
        return (page - 1) * pageSize;
    }

    public String orderByClause() {
        if (column == null) {
            return List.of("limit", "" + pageSize, "offset", "" + this.getOffset())
                    .stream().collect(Collectors.joining(" "));
        }
        return List
                .of(" order by", column, reverse ? "desc" : "asc", "limit", "" + pageSize, "offset",
                        "" + ((page - 1) * pageSize))
                .stream().collect(Collectors.joining(" "));
    }

    @SuppressWarnings("deprecation")
    public PageRequest toPageRequest() {
        if (column == null) {
            return new PageRequest(page, pageSize);
        }
        Sort sort = new Sort(reverse ? Sort.Direction.DESC : Sort.Direction.ASC, column);
        return new PageRequest(page - 1, pageSize, sort);
    }

}
