package com.example.facebookexample.utilities;


import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;

public class Maps {
   public static Map<String,Object> of(Object...args){
	   Map<String,Object> map=new LinkedHashMap<>();
	   for (int i = 0; i < args.length; i++) {  
		   map.put((String) args[i], args[++i]);
	   }
	   return map;
   }
   public static <K, V extends Comparable<? super V>> Entry<K, V> maxValueEntity(Map<K, V> map) {
		 return	Collections.max(map.entrySet(), (Entry<K, V> e1, Entry<K, V> e2) -> e1.getValue()
				        .compareTo(e2.getValue()));
		}
		public static <K extends Comparable<? super K>, V> Entry<K, V> maxKeyEntity(Map<K, V> map) {
			 return	Collections.max(map.entrySet(), (Entry<K, V> e1, Entry<K, V> e2) -> e1.getKey()
					        .compareTo(e2.getKey()));
		}
		public static <K, V extends Comparable<? super V>> Entry<K, V> minValueEntity(Map<K, V> map) {
			 return	Collections.min(map.entrySet(), (Entry<K, V> e1, Entry<K, V> e2) -> e1.getValue()
					        .compareTo(e2.getValue()));
		}
		public static <K extends Comparable<? super K>, V> Entry<K, V> minKeyEntity(Map<K, V> map) {
			return	Collections.min(map.entrySet(), (Entry<K, V> e1, Entry<K, V> e2) -> e1.getKey()
						        .compareTo(e2.getKey()));
		}
		
		public static <K, V extends Comparable<? super V>> List<K> allKiesWithMaxValue(Map<K, V> map) {
		    if(map.isEmpty())
		        return Collections.emptyList();
		    V max = map.values().stream().max(Comparator.naturalOrder()).get();
		    return getAllKeysByValue(map,max);
		}
		public static <K, V extends Comparable<? super V>> List<K> allKiesWithMinxValue(Map<K, V> map) {
		    if(map.isEmpty())
		        return Collections.emptyList();
		    V min = map.values().stream().min(Comparator.naturalOrder()).get();
		    return getAllKeysByValue(map,min);
		}
		public static <K, V extends Comparable<? super V>> List<K> getAllKeysByValue(Map<K, V> map, V value) {
			  return map.entrySet().stream()
				        .filter(e -> e.getValue().equals(value))
				        .map(Map.Entry::getKey)
				        .collect(Collectors.toList());
		}
}
