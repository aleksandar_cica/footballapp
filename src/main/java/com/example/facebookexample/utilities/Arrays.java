package com.example.facebookexample.utilities;



import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.function.Predicate;


public class Arrays<T> {
  public static <T> int frequency(T[] array,T element,int from, int to){
	  int count=0;
	  Predicate<T> predicate=equalsPredicate(element);
	  for (int i = from; i < to; i++) {
		if(predicate.test(array[i])){
			count++;
		}
	  }
	  return count;
  }
  public static <T> int frequency(T[] array,T elemnt,int from){
	  return frequency(array, elemnt, from, array.length);
  } 
  public static <T> int frequency(T[] array,T elemnt){
	  return frequency(array, elemnt, 0, array.length);
  }
  public static <T> T mostFrequent(T[] array){
	 Map<T,Integer> map=new HashMap<T,Integer>();
	 for (T t : array) {
		if(map.containsKey(t)){
			map.put(t, map.get(t)+1);
		}else{
			map.put(t, 1);
		}
	 }
	return Maps.maxValueEntity(map).getKey();
  }
  public static <T extends Comparable<? super T>> T min(T[] array){
	  return  min(array, (a,b)-> a.compareTo(b));
  }
  public static <T extends Comparable<? super T>> T max(T[] array){
	  return max(array, (a,b)-> a.compareTo(b));
  }
  
  public static <T> T min(T[] array, Comparator<? super T> comparator){
	  if(array.length==0)
		  return null;
	  T min=array[0];
	  for (int i = 1; i < array.length; i++) {
		 if(comparator.compare(min, array[i])<0){
			 min=array[i];
		 }
	  }
	  return min;
  }

  public static <T> T max(T[] array, Comparator<? super T> comparator){
	  if(array.length==0)
		  return null;
	  T max=array[0];
	  for (int i = 1; i < array.length; i++) {
		 if(comparator.compare(max, array[i])>0){
			 max=array[i];
		 }
	  }
	  return max;
  }
  
  public static <T> int indexOf(T[] array,T element){
	  Predicate<T> predicate=equalsPredicate(element);
	  for (int i = 0; i < array.length; i++) {
		  if(predicate.test(array[i])){
			return i;
		}
	  }
	  return -1;
  }
  public static <T> boolean includes(T[] array,T element){
	 return indexOf(array, element)>-1; 
  }
  
  public static <T> int lastIndexOf(T[] array,T element){
	  Predicate<T> predicate=equalsPredicate(element);
	  for (int i = array.length-1; i >-1; i--) {
		if(predicate.test(array[i])){
			return i;
		}
	  }
	  return -1;
  }
  public static<T> int findIndex(T[] array,Predicate<? super T> predicate){
	  for (int i = 0; i < array.length; i++) {
		if(predicate.test(array[i]))
			return i;
	  }
	  return -1;
  }
  
  public static<T> int findLastIndex(T[] array,Predicate<? super T> predicate){
	  for (int i = array.length-1; i >-1; i--) {
		if(predicate.test(array[i]))
			return i;
	  }
	  return -1;
  }
  
  public static<T> void sort (T[] array, Comparator<? super T> comparator){
	  for (int i = 0; i < array.length-1; i++) {
		for (int j = i+1; j < array.length; j++) {
			if(comparator.compare(array[i],array[j])>0){
			  swapElements(array, i, j);
			}
		}
	  }
   }
  public static <T extends Comparable<? super T>> void sort (T[] array){
	  sort(array, (a,b)-> a.compareTo(b));
  }
  public static <T> void reverse (T[] array){
	  for (int i = 0,j=array.length-1; i<j; i++,j--) {
		  swapElements(array, i, j);
	  }
  }
  
  public static <T> void swapElements(T[] array,int i, int j){
	    T temp=array[i];
		array[i]=array[j];
		array[j]=temp;
  }
  
  public static <T> void repaleceAllOccurance(T[] array, T oldEmement,T newElement){
	  Predicate<T> predicate=equalsPredicate(oldEmement);
	  for (int i = array.length-1; i >-1; i--) {
		if(predicate.test(array[i])){
			array[i]=newElement;
		}
	  }
  }
  
  public static <T> int count(T[] array, Predicate<? super T> predicate){
	  int count=0;
	  for(T element:array){
		  if(predicate.test(element))
			  count++;
	  }
	  return count;
  }
  
  public static <T> double sumDoubles(T[] array, Function<? super T,Double> function){
	  double sum=0;
	  for(T element:array){
		  if(element!=null){
		    sum+=function.apply(element);
		  }
	  }
	  return sum;
  }
  
  public static <T> double sumInts(T[] array, Function<T,Integer> function){
	  double sum=0;
	  for(T element:array){
		  if(element!=null){
			  sum+=function.apply(element);
		  }
	  }
	  return sum;
  }
  
  public static <T,K> Map<K,List<T>> groupBy(T[] array, Function<T,K> function){
	  Map<K,List<T>> groups=new HashMap<>();
	  for (T t : array) {
		  K key=function.apply(t);
		  List<T> list= groups.get(key);
		  if(list==null){
			  list=new ArrayList<T>();
		  }
		  list.add(t);
		  groups.put(key, list);
	  }
	  return groups;
  }
  
  private static <T> Predicate<T> equalsPredicate(T element){
	  Predicate<T> predicate;
	  if(element==null){
	    predicate= e-> e==null;
	  }else{
	    predicate= e-> element.equals(e);
	  }
	  return predicate;
  }
}

