package com.example.facebookexample.utilities;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.FORBIDDEN, reason = "Method is forbidden")
public class ForbiddenMethodException extends Exception {
}
