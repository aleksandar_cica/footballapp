package com.example.facebookexample.utilities;

import java.text.NumberFormat;
import java.util.Locale;

public class NumberFormatter {
	private static final NumberFormat formatter = NumberFormat.getCurrencyInstance(Locale.UK);
	public static String formatMoney(long money){
		return formatter.format(money);
	}
	public static String formatMoney(String money){
		return formatter.format(Long.parseLong(money));
	}
}
