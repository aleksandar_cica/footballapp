package com.example.facebookexample.utilities;

import java.time.LocalDateTime;
import java.util.Date;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import com.example.facebookexample.dates.Dates;

@Converter(autoApply = true)
public class LocalDateAttributeConverter implements AttributeConverter<LocalDateTime, Date> {

    @Override
    public Date convertToDatabaseColumn(LocalDateTime locDate) {
        return Dates.convertLocalDateTimeToDate(locDate);
    }

    @Override
    public LocalDateTime convertToEntityAttribute(Date date) {
        return Dates.convertDateToLocalDateTime(date);
    }
}
