package com.example.facebookexample.utilities;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import com.google.gson.JsonSyntaxException;

public class PropertiesSetter {

    public static boolean patch(Object target, Map<String, Object> map) {
        Class<?> class1 = target.getClass();
        while (class1 != Object.class) {
            for (Entry<String, Object> ent : map.entrySet()) {
                String key = ent.getKey();
                Object v = ent.getValue();
                try {
                    Field field = class1.getDeclaredField(key);
                    field.setAccessible(true);
                    try {
                        if (field.getType().isPrimitive()) {
                            switch (field.getType().getName().toLowerCase()) {
                                case "byte":
                                    field.set(target, (byte) (int) v);
                                    break;
                                case "short":
                                    field.set(target, (short) (int) v);
                                    break;
                                case "boolean":
                                    field.set(target, (boolean) v);
                                    break;
                                case "int":
                                    field.set(target, (int) v);
                                    break;
                                case "char":
                                    field.set(target, (char) (int) v);
                                    break;
                                case "double":
                                    field.set(target, (double) v);
                                    break;
                                case "long":
                                    field.set(target, (long) v);
                                    break;
                                case "float":
                                    field.set(target, (float) v);
                                    break;
                            }
                        } else {
                            if (field.getType() == String.class) {
                                field.set(target, (String) v);
                            } else if (field.getType() == LocalDateTime.class) {
                                field.set(target, (LocalDateTime) v);
                            } else if (field.getType() == Date.class) {
                                field.set(target, (Date) v);
                            }
                        }
                    } catch (JsonSyntaxException | IllegalArgumentException | IllegalAccessException e) {
                        return false;
                    }
                } catch (NoSuchFieldException | SecurityException e) {
                    return false;
                }
            }
            class1 = class1.getSuperclass();
        }
        return true;
    }

    public static <M> M set(Object object, Class<M> cl) {
        try {
            M mod = cl.newInstance();
            Map<String, Object> map = new HashMap<>();
            for (Field f : cl.getFields()) {
                f.setAccessible(true);
                map.put(f.getName(), f.get(object));
            }
            patch(mod, map);
            return mod;
        } catch (InstantiationException | IllegalAccessException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static boolean setProperties(Object object, Map<String, Object> map) {
        Field field;
        try {
            for (Map.Entry<String, Object> e : map.entrySet()) {
                field = object.getClass().getDeclaredField(e.getKey());
                field.setAccessible(true);
                try {
                    if (field.getType().isPrimitive()) {
                        if (field.getType().equals(Integer.class)) {
                            Method met = findByMethodName(field.getName(), object);
                            met.invoke(object, (int) e.getValue());
                        } else if (field.getType().equals(Byte.class)) {
                            Method met = findByMethodName(field.getName(), object);
                            System.out.println((byte) e.getValue());
                            met.invoke(object, (byte) e.getValue());
                        }
                    }
                    field.set(object, e.getValue());
                } catch (IllegalArgumentException | IllegalAccessException e1) {
                } catch (InvocationTargetException e1) {
                }
            }
        } catch (NoSuchFieldException | SecurityException e1) {
            e1.printStackTrace();
        }
        return true;
    }

    private static Method findByMethodName(String name, Object o) {
        for (Method method : o.getClass().getDeclaredMethods()) {
            if (method.getName().substring(3).equalsIgnoreCase(name)) {
                return method;
            }
        }
        return null;
    }
}
