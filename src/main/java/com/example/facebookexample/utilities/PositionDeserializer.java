package com.example.facebookexample.utilities;

import java.io.IOException;

import com.example.facebookexample.model.FootballPlayer;
import com.example.facebookexample.model.FootballPlayer.Position;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;

public class PositionDeserializer extends JsonDeserializer<Position> {

    public Position convertStringToPosition(String position) {
        return position == null ? Position.Undefined : FootballPlayer.Position.valueOf(position);
    }

    @Override
    public Position deserialize(JsonParser jp, DeserializationContext dc) throws IOException, JsonProcessingException {
        return convertStringToPosition(jp.getText());
    }

}
