package com.example.facebookexample.model;

import javax.persistence.ColumnResult;
import javax.persistence.ConstructorResult;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.NamedNativeQuery;
import javax.persistence.OneToOne;
import javax.persistence.SqlResultSetMapping;

@Entity
@SqlResultSetMapping(
        name = "coachResultMapper",
        classes = {
            @ConstructorResult(
                    targetClass = Coach.class,
                    columns = {
                        @ColumnResult(name = "id", type = Integer.class),
                        @ColumnResult(name = "name", type = String.class),
                        @ColumnResult(name = "last_name", type = String.class),
                        @ColumnResult(name = "country", type = String.class),
                        @ColumnResult(name = "club_id", type = Integer.class),
                        @ColumnResult(name = "club_name", type = String.class)
                    })})
@NamedNativeQuery(
        name = "findById",
        query = "SELECT c.id, c.name, c.last_name, c.country, cl.id as club_id, cl.name as club_name  FROM coach c LEFT JOIN club cl ON c.club_id = cl.id  WHERE c.active = true AND c.id = ?1",
        resultSetMapping = "coachResultMapper")
public class Coach extends Person {

    private static final long serialVersionUID = 1L;

    public Coach() {
        super();
    }

    @OneToOne
    @JoinColumn(name = "club_id", nullable = true)
    private Club club;

    public Coach(Club club) {
        this.club = club;
    }

    public Coach(Integer id, String name, String lastName, String country, Integer clubId, String clubName) {
        super(id, name, lastName, country);
        if (clubId != null) {
            this.club = new Club(clubId, clubName);
        }

    }

    @Override
    public Club getClub() {
        return club;
    }

    public void setClub(Club club) {
        this.club = club;
    }

}
