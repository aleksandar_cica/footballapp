package com.example.facebookexample.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.example.facebookexample.annotations.DeleteModification;

@Entity
@DeleteModification
public class MainReferee implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private boolean active;
    @NotNull
    @Size(min = 2, max = 20, message = "number of characters must be between 2 and 20")
    private String name;
    @NotNull
    @Size(min = 2, max = 20, message = "number of characters must be between 2 and 20")
    private String lastName;
    @NotNull
    @Size(min = 2, max = 20, message = "number of characters must be between 2 and 20")
    private String town;
    @OneToMany(mappedBy = "mainReferee", fetch = FetchType.LAZY)
    private List<Game> listOfGames;

    public MainReferee() {
        super();
        active = true;
    }

    public MainReferee(Integer id, String name, String lastName, String town) {
        this();
        this.id = id;
        this.name = name;
        this.lastName = lastName;
        this.town = town;
    }

    public MainReferee(Integer id, String name, String lastName, String town, List<Game> listOfGames) {
        this(id, name, lastName, town);
        this.listOfGames = listOfGames;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getTown() {
        return town;
    }

    public void setTown(String town) {
        this.town = town;
    }

    public List<Game> getListOfGames() {
        return listOfGames;
    }

    public void setListOfGames(List<Game> listOfGames) {
        this.listOfGames = listOfGames;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

}
