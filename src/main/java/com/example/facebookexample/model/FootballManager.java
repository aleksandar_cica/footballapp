package com.example.facebookexample.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.ColumnResult;
import javax.persistence.ConstructorResult;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.NamedNativeQuery;
import javax.persistence.OneToMany;
import javax.persistence.SqlResultSetMapping;

import com.example.facebookexample.annotations.DeleteModification;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@SqlResultSetMapping(
        name = "managerResultMapper",
        classes = {
            @ConstructorResult(
                    targetClass = FootballManager.class,
                    columns = {
                        @ColumnResult(name = "id", type = Integer.class),
                        @ColumnResult(name = "name", type = String.class),
                        @ColumnResult(name = "last_name", type = String.class),
                        @ColumnResult(name = "country", type = String.class)
                    })})
@NamedNativeQuery(
        name = "findByPrimaryKey",
        query = "SELECT id, name, last_name, country FROM football_manager WHERE id = ?1 AND active = true",
        resultSetMapping = "managerResultMapper")

@DeleteModification
public class FootballManager extends Person {

    private static final long serialVersionUID = 198700L;

    @JsonIgnore
    @OneToMany(mappedBy = "manager", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    List<FootballPlayer> players;

    public FootballManager() {
        super();
    }

    public FootballManager(int id, String name, String lastName, String country) {
        super(id, name, lastName, country);
    }

    public void setPlayers(List<FootballPlayer> players) {
        this.players = players;
    }

    public List<FootballPlayer> getPlayers() {
        return players;
    }

    @Override
    public Club getClub() {
        return null;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((players == null) ? 0 : players.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!super.equals(obj)) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        FootballManager other = (FootballManager) obj;
        if (players == null) {
            if (other.players != null) {
                return false;
            }
        } else if (!players.equals(other.players)) {
            return false;
        }
        return true;
    }

}
