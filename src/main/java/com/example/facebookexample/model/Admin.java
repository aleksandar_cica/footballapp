package com.example.facebookexample.model;

import javax.persistence.Entity;

@Entity
public class Admin extends AdminUser {

    private static final long serialVersionUID = 5110306931781573441L;

    public Admin() {
        super();
    }

    public String getUserType() {
        return "Admin";
    }

}
