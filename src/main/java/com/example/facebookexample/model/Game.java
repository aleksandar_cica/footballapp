package com.example.facebookexample.model;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.ColumnDefault;

import com.example.facebookexample.dates.DefaultLocalDateTimeDeserializer;
import com.example.facebookexample.utilities.LocalDateAttributeConverter;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

@Entity
public class Game implements Serializable {

    private static final long serialVersionUID = 10876L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    @NotNull
    @Convert(converter = LocalDateAttributeConverter.class)
    @JsonDeserialize(using = DefaultLocalDateTimeDeserializer.class)
    private LocalDateTime date;

    @ManyToOne
    @JoinColumn(name = "h_id", nullable = false, updatable = false)
    Club home;

    @ManyToOne
    @JoinColumn(name = "g_id", nullable = false, updatable = false)
    Club guest;

    @ColumnDefault("0")
    @Min(value = 0, message = "min value is 0")
    @Column(name = "homegoals")
    private byte homeGoals;

    @ColumnDefault("0")
    @Min(value = 0, message = "min value is 0")
    @Column(name = "guestgoals")
    private byte guestGoals;

    @ManyToOne
    @JoinColumn(name = "mainReferee_id", referencedColumnName = "id", nullable = false, updatable = false)
    private MainReferee mainReferee;

    public MainReferee getMainReferee() {
        return mainReferee;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    public Club getHome() {
        return home;
    }

    public void setHome(Club home) {
        this.home = home;
    }

    public Club getGuest() {
        return guest;
    }

    public void setGuest(Club guest) {
        this.guest = guest;
    }

    public byte getHomeGoals() {
        return homeGoals;
    }

    public void setHomeGoals(byte homeGoals) {
        this.homeGoals = homeGoals;
    }

    public byte getGuestGoals() {
        return guestGoals;
    }

    public void setGuestGoals(byte guestGoals) {
        this.guestGoals = guestGoals;
    }

    public void setMainReferee(MainReferee mainReferee) {
        this.mainReferee = mainReferee;
    }
}
