package com.example.facebookexample.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.ColumnResult;
import javax.persistence.ConstructorResult;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.NamedNativeQuery;
import javax.persistence.SqlResultSetMapping;

@Entity
@SqlResultSetMapping(
        name = "roleResultMapper",
        classes = {
            @ConstructorResult(
                    targetClass = Role.class,
                    columns = {
                        @ColumnResult(name = "id", type = Integer.class),
                        @ColumnResult(name = "role", type = String.class),
                        @ColumnResult(name = "description", type = String.class),})})
@NamedNativeQuery(
        name = "findAllRoles",
        query = "SELECT id, role, description FROM role",
        resultSetMapping = "roleResultMapper")
public class Role {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    private PermissionsEnum role;

    private String description;

    @ManyToMany(mappedBy = "roles")
    private List<AdminUser> users;

    public Role() {

    }

    public Role(Integer id, PermissionsEnum role, String description) {
        this.id = id;
        this.role = role;
        this.description = description;
    }

    public Role(Integer id, String role, String description) {
        this(id, PermissionsEnum.valueOf(role), description);
    }

    public PermissionsEnum getRole() {
        return role;
    }

    public void setRole(PermissionsEnum role) {
        this.role = role;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public List<AdminUser> getUsers() {
        return users;
    }

    public void setUsers(List<AdminUser> users) {
        this.users = users;
    }

}
