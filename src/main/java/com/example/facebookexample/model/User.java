package com.example.facebookexample.model;

import javax.persistence.Entity;

@Entity
public class User extends AdminUser {

    private static final long serialVersionUID = 6366764704988177401L;

    public User() {
        super();
    }

    public String getUserType() {
        return "User";
    }
}
