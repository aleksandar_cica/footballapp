package com.example.facebookexample.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Transient;

import com.example.facebookexample.annotations.DeleteModification;
import com.example.facebookexample.annotations.NotFormField;
import com.example.facebookexample.utilities.PositionDeserializer;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

@Entity
@DeleteModification
public class FootballPlayer extends Person {

    public static enum Position {
        Defender, Midfielder, Striker, Undefined, Goalkeeper
    }

    private static final long serialVersionUID = 1L;
    @NotFormField
    @ManyToOne(optional = true, fetch = FetchType.LAZY)
    @JoinColumn(name = "manager", referencedColumnName = "id")
    private FootballManager manager;
    @NotFormField
    @OneToMany(mappedBy = "player", fetch = FetchType.LAZY, cascade = CascadeType.REFRESH)
    @OrderBy("transferDate ASC")
    private List<Transfer> transfers = new ArrayList<>();
    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    @JsonDeserialize(using = PositionDeserializer.class)
    private FootballPlayer.Position position;
    @Transient
    private Club club;

    public FootballPlayer() {
        super();
        position = FootballPlayer.Position.Undefined;
    }

    public FootballPlayer(Integer id, String name, String lastName, String country) {
        super(id, name, lastName, country);
        position = FootballPlayer.Position.Undefined;
    }

    public FootballPlayer(Integer id, String name, String lastName, String country, FootballManager manager) {
        super(id, name, lastName, country);
        this.manager = manager;
        position = FootballPlayer.Position.Undefined;
    }

    public FootballPlayer.Position getPosition() {
        return position;
    }

    public void setPosition(FootballPlayer.Position position) {
        this.position = position;
    }

    public FootballManager getMeager() {
        return manager;
    }

    public void setTransfers(List<Transfer> transfers) {
        this.transfers = transfers;
    }

    public List<Transfer> getTransfers() {
        return transfers;
    }

    public FootballManager getManager() {
        return manager;
    }

    public void setManager(FootballManager manager) {
        this.manager = manager;
    }

    @Override
    public Club getClub() {
        return club;
    }

    public void setClub(Club club) {
        this.club = club;
    }

}
