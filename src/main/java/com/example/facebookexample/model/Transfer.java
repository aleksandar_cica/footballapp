package com.example.facebookexample.model;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.Min;

import com.example.facebookexample.utilities.LocalDateAttributeConverter;

@Entity
@Table(name = "transfers")
public class Transfer implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    @Column(nullable = false, name = "date", updatable = false)
    @Convert(converter = LocalDateAttributeConverter.class)
    private LocalDateTime transferDate;
    @Min(0)
    private int money;
    @ManyToOne(optional = true)
    @JoinColumn(name = "sailer_id", updatable = false, nullable = true)
    private Club sailerClub;

    @ManyToOne
    @JoinColumn(name = "buyer_id", updatable = false, nullable = false)
    private Club buyerClub;

    @ManyToOne
    @JoinColumn(name = "player_id", updatable = false, nullable = false)
    private FootballPlayer player;

    public Transfer() {
        transferDate = LocalDateTime.now();
    }

    public Transfer(Club sailerClub, Club buyerClub, FootballPlayer player, int money) {
        this();
        this.sailerClub = sailerClub;
        this.buyerClub = buyerClub;
        this.player = player;
        this.money = money;
    }

    @Override
    public int hashCode() {
        return id;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        Transfer other = (Transfer) obj;
        return Objects.equals(id, other.id);
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public LocalDateTime getTransferDate() {
        return transferDate;
    }

    public Club getBuyerClub() {
        return buyerClub;
    }

    public void setBuyerClub(Club bayerClub) {
        this.buyerClub = bayerClub;
    }

    public FootballPlayer getPlayer() {
        return player;
    }

    public void setPlayer(FootballPlayer player) {
        this.player = player;
    }

    public Club getSailerClub() {
        return sailerClub;
    }

    public void setSailerClub(Club sailerClub) {
        this.sailerClub = sailerClub;
    }

    public int getMoney() {
        return money;
    }

    public void setTransferDate(LocalDateTime transferDate) {
        this.transferDate = transferDate;
    }

    public void setMoney(int money) {
        this.money = money;
    }
}
