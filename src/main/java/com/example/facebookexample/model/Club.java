package com.example.facebookexample.model;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;

import javax.persistence.CascadeType;
import javax.persistence.ColumnResult;
import javax.persistence.ConstructorResult;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedNativeQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.Size;

@Entity
@Table(uniqueConstraints = {
    @UniqueConstraint(columnNames = {"name"})})

@SqlResultSetMapping(
        name = "baseClubMapper",
        classes = {
            @ConstructorResult(
                    targetClass = Club.class,
                    columns = {
                        @ColumnResult(name = "id", type = Integer.class),
                        @ColumnResult(name = "name", type = String.class),
                        @ColumnResult(name = "city", type = String.class)
                    })})
@NamedNativeQuery(
        name = "findByName",
        query = "SELECT id, name, city FROM club WHERE LOWER(name) like LOWER(CONCAT('%', ?1, '%')) order by name limit ?2 offset ?3",
        resultSetMapping = "baseClubMapper")

@SqlResultSetMapping(
        name = "clubMapper",
        classes = {
            @ConstructorResult(
                    targetClass = Club.class,
                    columns = {
                        @ColumnResult(name = "id", type = Integer.class),
                        @ColumnResult(name = "name", type = String.class),
                        @ColumnResult(name = "city", type = String.class),
                        @ColumnResult(name = "c_id", type = Integer.class),
                        @ColumnResult(name = "c_name", type = String.class),
                        @ColumnResult(name = "c_last_name", type = String.class),
                        @ColumnResult(name = "c_country", type = String.class)
                    })})
@NamedNativeQuery(
        name = "findClubById",
        query = "SELECT cl.id, cl.name, cl.city, c.id as c_id, c.name as c_name, c.last_name as c_last_name, "
        + "c.country as c_country FROM club cl LEFT JOIN coach c ON c.club_id = cl.id WHERE cl.id = ?",
        resultSetMapping = "clubMapper")
public class Club implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    @Size(min = 3, max = 20)
    private String city;
    @Size(min = 3, max = 15)
    private String name;

    @OneToOne(fetch = FetchType.LAZY, cascade = {CascadeType.MERGE, CascadeType.PERSIST}, mappedBy = "club")
    private Coach coach;

    @OneToMany(mappedBy = "home", fetch = FetchType.LAZY, cascade = CascadeType.REFRESH)
    private List<Game> homeGames;

    @OneToMany(mappedBy = "guest", fetch = FetchType.LAZY, cascade = CascadeType.REFRESH)
    private List<Game> guestGames;

    @OneToMany(mappedBy = "sailerClub", fetch = FetchType.LAZY, cascade = CascadeType.REFRESH)
    private List<Transfer> soldPlayers;

    @OneToMany(mappedBy = "buyerClub", fetch = FetchType.LAZY, cascade = {CascadeType.MERGE, CascadeType.PERSIST})
    private List<Transfer> bayedPlayers;

    public Club() {
    }

    public Club(String name) {
        this();
        this.name = name;
    }

    public Club(Integer id, String name) {
        this();
        this.name = name;
        this.id = id;
    }

    public Club(Integer id, String name, String city) {
        this(id, name);
        this.city = city;
    }

    public Club(Integer id, String name, String city, Integer coachId, String coachName, String coachLastName, String coachCountry) {
        this(id, name, city);
        if (coachId != null) {
            this.coach = new Coach(coachId, coachName, coachLastName, coachCountry, id, name);
        }
    }

    public Club(Integer id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        return id;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        Club other = (Club) obj;
        return Objects.equals(id, other.id);
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Game> getHomeGames() {
        return homeGames;
    }

    public void setHomeGames(List<Game> homeGames) {
        this.homeGames = homeGames;
    }

    public List<Game> getGuestGames() {
        return guestGames;
    }

    public void setGuestGames(List<Game> guestGames) {
        this.guestGames = guestGames;
    }

    public List<Transfer> getSoldPlayers() {
        return soldPlayers;
    }

    public List<Transfer> getBayedPlayers() {
        return bayedPlayers;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public Coach getCoach() {
        return coach;
    }

    public void setCoach(Coach coach) {
        this.coach = coach;
    }

    public void setSoldPlayers(List<Transfer> soldPlayers) {
        this.soldPlayers = soldPlayers;
    }

    public void setBayedPlayers(List<Transfer> bayedPlayers) {
        this.bayedPlayers = bayedPlayers;
    }

}
